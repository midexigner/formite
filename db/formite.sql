-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2018 at 01:48 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `formite`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `ID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `Heading` text NOT NULL,
  `Image1` text NOT NULL,
  `Image2` text NOT NULL,
  `Image` text NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`ID`, `Text`, `Heading`, `Image1`, `Image2`, `Image`, `PerformedBy`, `DateModified`) VALUES
(1, '<p class="red">Magnificent Craftiness</p>\r\n<p>Having evolved as a market leader in laminating industry, Formite is highly focused towards constructing durable and exquisite surroundings that enhance the customer&rsquo;s standard of living.</p>\r\n<p>Our eye-catching array of product designs are premeditated to make certain that our clientele is delivered the promised lifestyle they deserve.</p>', 'WELCOME TO FORMITE', 'about1.jpg', '', '', 4, '2018-03-13 16:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `ID` int(11) NOT NULL,
  `Heading` varchar(255) NOT NULL,
  `Image` text NOT NULL,
  `HomePage` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`ID`, `Heading`, `Image`, `HomePage`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(5, 'Home Packages', '5.jpg', 1, 1, 0, 4, '2017-11-23 14:32:21', '2017-12-22 17:15:17'),
(6, 'Parallex company page', '6.png', 0, 1, 0, 4, '2017-11-23 15:06:32', '2017-12-22 17:35:59'),
(10, 'Parallex package page', '10.jpg', 0, 1, 0, 4, '2017-11-23 16:00:15', '2017-12-22 17:36:35'),
(11, 'Pages head', '11.png', 0, 1, 0, 4, '2017-11-23 16:00:29', '2017-12-22 17:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `bestseller`
--

CREATE TABLE IF NOT EXISTS `bestseller` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Headingtwo` text NOT NULL,
  `Headingthree` text NOT NULL,
  `Slider` text NOT NULL,
  `CornerTag` varchar(255) DEFAULT NULL,
  `RightImage` varchar(255) DEFAULT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bestseller`
--

INSERT INTO `bestseller` (`ID`, `Name`, `Heading`, `Headingtwo`, `Headingthree`, `Slider`, `CornerTag`, `RightImage`, `URL`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, '', 'Marble - 8106', 'Solid - 7072', 'Pattern - 8727', '1.jpg', '1_1.jpg', '1_2.jpg', '', 1, 0, 4, '2018-03-15 11:34:43', '0000-00-00 00:00:00'),
(2, '', 'Woodgrain - 7124', 'Solid - 7047', 'Pattern - 8626', '2.jpg', '2_1.jpg', '2_2.jpg', '', 1, 0, 4, '2018-03-15 11:38:23', '0000-00-00 00:00:00'),
(3, '', 'Woodgrain - 7196', 'Marble - 8145', 'Solid - 7076', '3.jpg', '3_1.jpg', '3_2.jpg', '', 1, 0, 4, '2018-03-15 11:40:11', '0000-00-00 00:00:00'),
(4, '', 'Marble - 8144', 'Pattern - 8446', 'Woodgrain - 7415', '4.jpg', '4_1.jpg', '4_2.jpg', '', 1, 0, 4, '2018-03-15 11:41:11', '0000-00-00 00:00:00'),
(5, '', 'Pattern - 8478', 'Plain - 7045', 'Woodgrain - 7413', '5.jpg', '5_1.jpg', '5_2.jpg', '', 1, 0, 4, '2018-03-15 11:42:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Email` text NOT NULL,
  `Phone` varchar(190) NOT NULL,
  `Company` text NOT NULL,
  `Message` text NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unread,1= read',
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`ID`, `Name`, `Email`, `Phone`, `Company`, `Message`, `Status`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(3, 'Jenna Hancock', 'syfeto@hotmail.com', '+256-26-2515754', 'Holland and Travis Plc', 'Labore in fugiat culpa enim', 1, '2018-03-14 14:58:02', '2018-03-14 14:58:30', 4),
(4, 'Alvin Hampton', 'vaxexovu@hotmail.com', '+427-44-2079716', 'Stafford Evans Traders', 'Velit sunt voluptates sunt qui ea voluptas quo ut quasi eum ullamco dolorem placeat sit quam fugit', 0, '2018-03-15 12:29:17', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallerycat`
--

CREATE TABLE IF NOT EXISTS `gallerycat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `performby` int(11) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datemodified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallerycat`
--

INSERT INTO `gallerycat` (`id`, `name`, `slug`, `content`, `image`, `status`, `performby`, `dateadded`, `datemodified`) VALUES
(1, 'Marble', 'marble', '<p>Formite brings you a whole new world of elegant and aesthetic marble textures.</p>', '1.jpg', 1, 4, '2018-03-15 12:07:43', NULL),
(2, 'METALLICA', 'metallica', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor magna aliqua.</p>', '2.jpg', 1, 4, '2018-03-15 12:09:31', NULL),
(3, 'WOODGRAIN', 'woodgrain', '<p>Explore a perfectly appealing range of Wood grain colors and surfaces.</p>', '3.jpg', 1, 4, '2018-03-15 12:10:07', NULL),
(4, 'Solid Colors', 'solid-colors', '<p>Delve into our ravishing selection of solid color range.</p>', '4.jpg', 1, 4, '2018-03-15 12:10:37', NULL),
(5, 'Patterns', 'patterns', '<p>Observe our extensive range of eye-catching patterns.</p>', '5.jpg', 1, 4, '2018-03-15 12:12:03', NULL),
(6, 'Custom Laminates', 'custom-laminates', '<p>Coming Soon ...</p>', '6.jpg', 1, 4, '2018-03-15 12:12:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallerylist`
--

CREATE TABLE IF NOT EXISTS `gallerylist` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Slider` text NOT NULL,
  `CornerTag` varchar(255) DEFAULT NULL,
  `RightImage` varchar(255) DEFAULT NULL,
  `Gallerycat` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallerylist`
--

INSERT INTO `gallerylist` (`ID`, `Name`, `Heading`, `Text`, `Slider`, `CornerTag`, `RightImage`, `Gallerycat`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(7, '', '8105', '', '7.jpg', NULL, NULL, 1, 1, 0, 4, '2018-03-15 16:49:27', '0000-00-00 00:00:00'),
(8, '', '8106', '', '8.jpg', NULL, NULL, 1, 1, 0, 4, '2018-03-16 09:02:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `getacall`
--

CREATE TABLE IF NOT EXISTS `getacall` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(80) NOT NULL,
  `interest` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unread,1= read',
  `PerformedBy` int(11) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datemodified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `getacall`
--

INSERT INTO `getacall` (`id`, `name`, `phone`, `interest`, `status`, `PerformedBy`, `dateadded`, `datemodified`) VALUES
(3, 'Fredericka Brady', '+873-81-4025731', 'APPLICATION', 1, 4, '2018-03-14 06:54:08', '2018-03-14 08:17:56'),
(4, 'Diana Randall', '+987-27-3561534', 'COMPACT LAMINATES', 0, 0, '2018-03-14 07:11:47', '0000-00-00 00:00:00'),
(5, 'Abel Vaughan', '+628-31-6675781', 'SPECIALITY LAMINATES', 0, 0, '2018-03-14 07:14:37', '0000-00-00 00:00:00'),
(6, 'Clayton Jefferson', '+856-48-2029556', 'HIGH PRESSURE LAMINATES', 0, 0, '2018-03-14 07:14:58', '0000-00-00 00:00:00'),
(10, 'Avram Rutledge', '+561-62-7941958', '', 0, 0, '2018-03-14 09:22:15', '0000-00-00 00:00:00'),
(11, 'Veronica Lindsay', '+812-35-2124592', 'Interested In', 1, 4, '2018-03-14 09:57:52', '2018-03-14 09:58:16'),
(12, 'Jaquelyn Espinoza', '+616-52-3386372', 'TECHNICAL LAMINATES', 0, 0, '2018-03-15 07:28:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `getacallhome`
--

CREATE TABLE IF NOT EXISTS `getacallhome` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(80) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unread,1= read',
  `PerformedBy` int(11) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datemodified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `getacallhome`
--

INSERT INTO `getacallhome` (`id`, `name`, `email`, `message`, `status`, `PerformedBy`, `dateadded`, `datemodified`) VALUES
(1, 'Aspernatur et veniam vel beatae dolore eos quia cumque quae nesciunt', 'gymyjus@yahoo.com', 'Adipisicing quae consequatur est possimus illo tenetur animi sit minus sapiente esse', 1, 4, '2018-03-14 10:13:17', '2018-03-14 10:33:00');

-- --------------------------------------------------------

--
-- Table structure for table `multimedia`
--

CREATE TABLE IF NOT EXISTS `multimedia` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `File` text NOT NULL,
  `Type` tinyint(1) NOT NULL,
  `DateAdded` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multimedia`
--

INSERT INTO `multimedia` (`ID`, `Name`, `File`, `Type`, `DateAdded`) VALUES
(25, 'mission', '25.jpg', 1, '2018-03-13 15:07:18'),
(26, 'mission info graphic', '26.jpg', 1, '2018-03-13 15:08:20'),
(27, 'intro-img.jpg', '27.jpg', 1, '2018-03-14 15:53:14'),
(28, 'Intro-img-2.jpg', '28.jpg', 1, '2018-03-14 15:54:09'),
(29, 'Intro-3.jpg', '29.jpg', 1, '2018-03-14 15:55:02'),
(30, 'gallery-banner-1', '30.jpg', 1, '2018-03-15 13:53:23'),
(31, 'product-inner-img-1', '31.jpg', 1, '2018-03-17 16:43:40');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Status`, `DateAdded`, `DateModified`) VALUES
(5, '', '', 'admin@admin.com', 1, '2017-11-24 14:49:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Slug` varchar(230) NOT NULL,
  `Heading` text NOT NULL,
  `Tagline` text NOT NULL,
  `Image` text NOT NULL,
  `Text` text NOT NULL,
  `Sort` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`ID`, `Name`, `Slug`, `Heading`, `Tagline`, `Image`, `Text`, `Sort`, `Status`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(4, '', 'mission', 'OUR MISSION', 'Mission', '4.jpg', '<section id="content">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-6">\r\n<div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">\r\n<h3 class="heading-title red">Mission</h3>\r\n<p>We bring joy to our customers by creating beautiful interiors &amp; enhancing their lifestyles while being the leading brand in the decorative surface industry, offering innovative &amp; creative designs in both local and international markets.</p>\r\n</div>\r\n</div>\r\n<div class="col-md-6">\r\n<div class="mission"><img class="img-responsive" src="admin/assets/multimedia-images/25.jpg" alt="mission-img" /></div>\r\n</div>\r\n</div>\r\n<div class="row">\r\n<div class="col-md-12 mission-one"><img class="wow zoomIn" src="admin/assets/multimedia-images/26.jpg" alt="mission" data-wow-duration="0.3s" data-wow-delay="0.4s" /></div>\r\n</div>\r\n</div>\r\n</section>', 0, 1, 4, '2018-03-13 15:04:43', '2018-03-13 17:51:00'),
(7, '', 'contact-us', 'We''d love to hear from you!', 'CONTACT US', '7.jpg', '<h3>Get In Touch</h3>\r\n<p>Want us to give you a call? <br /><br /> We will do our best to meet your prerequisites and spending plan</p>\r\n<div class="media">\r\n<div class="media-left">&nbsp;</div>\r\n<div class="media-body  media-middle">\r\n<p><a href="mailto:info@formite.com.pk">info@formite.com.pk</a></p>\r\n</div>\r\n</div>\r\n<div class="media">\r\n<div class="media-left">&nbsp;</div>\r\n<div class="media-body">\r\n<h4 class="media-heading">HEAD OFFICE</h4>\r\n4th Floor, House of Habib, Plot-3, JCHS, Block 7/8, <br />Shahra-e-Faisal, Karachi - Pakistan.<br /> Tel: +92 213 4545 195-6</div>\r\n</div>\r\n<div class="media">\r\n<div class="media-left">&nbsp;</div>\r\n<div class="media-body">\r\n<h4 class="media-heading">FACTORY OUTLET I</h4>\r\nTimber Market, Siddiq Wahab Road, Karachi - Pakistan.<br /> Tel: +92 213 2723 335</div>\r\n</div>\r\n<div class="media">\r\n<div class="media-left">&nbsp;</div>\r\n<div class="media-body">\r\n<h4 class="media-heading">FACTORY OUTLET II</h4>\r\nHouse # G-370, Gali # 2, Sir Syed Ahmed Khan <br />Road, Manzoor Colony, Mehmoodabad, <br />Karachi - Pakistan. Tel: +92 213 5887 624</div>\r\n</div>\r\n<div class="media">\r\n<div class="media-left">&nbsp;</div>\r\n<div class="media-body">\r\n<h4 class="media-heading">FORMITE DISPLAY CENTER</h4>\r\nShop #6 G3/1 Clifton Pride, Block 8, KDA <br />Scheme 5, Clifton, Karachi - Pakistan.<br /> Tel: +92 213 5865 601-2</div>\r\n</div>', 0, 1, 4, '2018-03-13 17:27:00', '2018-03-14 09:23:32'),
(9, '', 'our-partners', 'COMMITMENT TO EMINENCE', 'OUR PARTNERS', '9.jpg', '<p>Formite partners with the leading brands to craft intricate interplays of innovative solutions that surpass client&rsquo;s expectation</p>', 0, 1, 4, '2018-03-14 15:44:42', '2018-03-14 15:49:57'),
(10, '', 'introduction', 'Introduction', 'Introduction', '10.jpg', '<section class="introduction" id="content">\r\n<div class="container">\r\n<div class="row">\r\n<div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">\r\n<h3>We Explore, We Pioneer</h3>\r\n<p>One of the prime leading brands in the high pressure laminates (HPL) industries of Pakistan enduring since 1980. Formerly recognized as Baluchistan Laminates Division and an established brand of Thal limited, Formite is the country&rsquo;s largest laminate manufacturing and supplying brand that stands out for its ingenious concepts to meet client&rsquo;s expectations.</p>\r\n</div>\r\n<div class="row">\r\n<div class="col-md-3">\r\n<div class="tc-image-effect-shine"><img class="img-responsive" alt="" src="admin/assets/multimedia-images/27.jpg" /></div>\r\n</div>\r\n<div class="col-md-9">\r\n<h5>ACCOMMODATING NEEDS</h5>\r\n<p>Breaking the nexus of ingenuousness, Formite is where a myriad of creative possibilities are existent for clients to explore the depths of imagination.</p>\r\n<br />\r\n<p>Distinctively embellished laminate surfaces, that are durable, wear &amp; heat resistant, designed for horizontal and vertical applications by state of the art technology make Formite the preferred choice for a modern day clientele.</p>\r\n<br />\r\n<p>With our far-reaching array of ideas and services to transform spaces into alluring designs we create a mix and match of solids, graphic patterns and finishes.</p>\r\n<h5>&nbsp;</h5>\r\n<p>&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class="white-bg" id="content-one">\r\n<div class="container">\r\n<div class="row">\r\n<div class="heading-para wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">\r\n<div class="col-md-4">Exquisite<span class="one">design,</span> <span class="two">unparalleled</span> <span class="three">technology,</span> <span class="four">unmatched </span> <span class="two">quality</span></div>\r\n</div>\r\n<div class="col-md-8"><img class="img-responsive" alt="" src="admin/assets/multimedia-images/28.jpg" /></div>\r\n</div>\r\n<div class="row">\r\n<h3>Accommodating Style</h3>\r\n<p>Rich and profound textures complemented by incisive interplays of patterns and thoughtful designs to bring forth a variety of laminates reflecting the latest trends. Our efficient planning processes along with supreme quality of contemporary laminate products, allow our customers to exquisitely renovate their interiors as envisioned without exceeding the budget.</p>\r\n<div class="tc-image-effect-shine"><img alt="" src="admin/assets/multimedia-images/29.jpg" /></div>\r\n</div>\r\n</div>\r\n</section>', 0, 1, 4, '2018-03-14 15:55:16', '2018-03-14 16:03:13'),
(11, '', 'timeline-achive', 'FROM EMERGENCE TO EXCELLENCE', 'TIMELINE OF ACHIEVEMENTS', '11.jpg', '<p>Since 1980, Fomite, a brand of Baluchistan Laminates is proud to give you all it takes to make your interiors wow. As the largest manufacturer and supplier of decorative &amp; technical laminates in Pakistan, our tradition to transform ambiances continues. Year after year, we deliver on the same promise, and here we live it again. This is to challenging your imagination. This is to making homes and offices, spaces and buildings speak loud and beyond. Let&rsquo;s do it together</p>', 0, 1, 4, '2018-03-14 17:23:06', '0000-00-00 00:00:00'),
(12, '', 'gallery', 'Our Gallery', 'Gallery', '12.jpg', '<div class="row">\r\n<div class="heading wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.6s">\r\n<h3 class="heading-title">We accommodate your spaces with thoughtful designs</h3>\r\n<p>Prepare to be inspired by our exclusive range of mesmerizing patterns and colors.</p>\r\n</div>\r\n</div>\r\n<div class="row">\r\n<div class="inner-gallery-banner tc-image-effect-shine"><a href="new-finishes"><img src="admin/assets/multimedia-images/30.jpg" alt="gallery-banner-1" /></a></div>\r\n</div>', 0, 1, 4, '2018-03-15 13:54:19', '0000-00-00 00:00:00'),
(13, '', 'products', 'Our Products', 'products', '13.jpg', '<div class="row">\r\n<div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">\r\n<h3 class="red text-uppercase">Capture the essence of perfection with Formite</h3>\r\n<p>Explore our spectrum of tasteful designs and myriad possibilities reflecting the latest trends.</p>\r\n</div>\r\n</div>', 0, 1, 4, '2018-03-17 11:54:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Slider` text NOT NULL,
  `CornerTag` varchar(255) DEFAULT NULL,
  `RightImage` varchar(255) DEFAULT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`ID`, `Name`, `Heading`, `Text`, `Slider`, `CornerTag`, `RightImage`, `URL`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(1, '', '', '', '1.png', NULL, NULL, '', 1, 0, 4, '2018-03-14 16:41:24', '0000-00-00 00:00:00'),
(2, '', '', '', '2.png', NULL, NULL, '', 1, 0, 4, '2018-03-14 16:47:04', '0000-00-00 00:00:00'),
(3, '', '', '', '3.png', NULL, NULL, '', 1, 0, 4, '2018-03-14 16:47:11', '0000-00-00 00:00:00'),
(4, '', '', '', '4.png', NULL, NULL, '', 1, 0, 4, '2018-03-14 16:47:22', '0000-00-00 00:00:00'),
(5, '', '', '', '5.png', NULL, NULL, '', 1, 0, 4, '2018-03-14 16:47:28', '0000-00-00 00:00:00'),
(6, '', '', '', '6.png', NULL, NULL, '', 1, 0, 4, '2018-03-14 16:47:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `productcat`
--

CREATE TABLE IF NOT EXISTS `productcat` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `performby` int(11) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datemodified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productcat`
--

INSERT INTO `productcat` (`id`, `name`, `slug`, `content`, `image`, `status`, `performby`, `dateadded`, `datemodified`) VALUES
(1, 'High Pressure Laminates', 'high-pressure-laminates', '<p>Our ever growing brand, Formite, envisions on creating top-quality laminates by employing state of the art processes.</p>', '1.jpg', 1, 4, '2018-03-17 12:23:30', NULL),
(2, 'Low Pressure Laminates', 'low-pressure-laminates', '<p>Best-matched edge solutions to interior decorations, low pressure laminates are ideal surfaces with pristine finishing and functionality&nbsp;</p>', '2.jpg', 1, 4, '2018-03-17 05:37:12', NULL),
(3, 'COMPACT LAMINATES', 'compact-laminates', '<p>Originally having its significance as an embellished decorative grade material, compact laminates evolved as industrial grade ...</p>', '3.jpg', 1, 4, '2018-03-17 05:39:55', NULL),
(4, 'TECHNICAL LAMINATES', 'technical-laminates', '<p>Grade P1 &ndash; The paper based P1 (PFCP 201) has mechanical applications and mechanical properties far better than other PFCP types</p>', '4.jpg', 1, 4, '2018-03-17 05:41:53', NULL),
(5, 'SPECIALITY LAMINATES', 'speciality-laminates', '<p>Fibrite &ndash; Synthetic Resin Bonded Fabric Laminate, also acknowledged as &lsquo;FIBRITE&rsquo; has its application for both mechanical and ...</p>', '5.jpg', 1, 4, '2018-03-17 05:44:33', NULL),
(6, 'Applications', 'applications', '<p>Coming Soon</p>', '6.jpg', 1, 4, '2018-03-17 05:53:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Website` text NOT NULL,
  `Email` text NOT NULL,
  `Phone` text NOT NULL,
  `Mobile` text NOT NULL,
  `Fax` text NOT NULL,
  `Address` text NOT NULL,
  `FooterAbout` text NOT NULL,
  `Photo` text NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`ID`, `Name`, `Website`, `Email`, `Phone`, `Mobile`, `Fax`, `Address`, `FooterAbout`, `Photo`, `DateModified`, `PerformedBy`) VALUES
(1, 'Formite', 'www.formite.com.pk', 'info@formite.com.pk', '+92 213 4545 195-6', '', 'UNIQUENESS IN EXCLUSIVITY', '4th Floor, House of Habib, Plot-3, JCHS, Block 7/8, Shahra-e-Faisal, Karachi, Pakistan', 'A pioneer and market leader in the laminates industry, Formite provides its client an interior, that is unique and exclusive, to surpass their expectations by having acquired state of the art technology and defined efficient planning processes', '1.png', '2018-03-14 17:19:47', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Slider` text NOT NULL,
  `CornerTag` varchar(255) DEFAULT NULL,
  `RightImage` varchar(255) DEFAULT NULL,
  `URL` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`ID`, `Name`, `Heading`, `Text`, `Slider`, `CornerTag`, `RightImage`, `URL`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(9, '', 'INFINITE POSSIBILITIES', '<p>Transforming spaces into alluring designs</p>', '9.jpg', NULL, NULL, '', 1, 0, 4, '2018-03-14 17:07:41', '0000-00-00 00:00:00'),
(10, '', 'DESIGN WITH ELEGANCE', '', '10.jpg', NULL, NULL, '', 1, 0, 4, '2018-03-14 17:07:59', '0000-00-00 00:00:00'),
(11, '', 'STYLE YOUR SPACE', '', '11.jpg', NULL, NULL, '', 1, 0, 4, '2018-03-14 17:08:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sociallinks`
--

CREATE TABLE IF NOT EXISTS `sociallinks` (
  `ID` int(11) NOT NULL,
  `Heading` text NOT NULL,
  `Text` text NOT NULL,
  `Image` text NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Sort` int(11) NOT NULL,
  `PerformedBy` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sociallinks`
--

INSERT INTO `sociallinks` (`ID`, `Heading`, `Text`, `Image`, `Status`, `Sort`, `PerformedBy`, `DateAdded`, `DateModified`) VALUES
(27, 'facebook', 'facebook', '27.png', 1, 0, 4, '2017-12-22 17:02:36', '2018-03-13 16:58:08'),
(28, 'twitter', 'twitter', '28.png', 1, 0, 4, '2017-12-22 17:09:14', '2018-03-13 16:58:40'),
(29, 'instagram', 'instagram', '29.png', 1, 0, 4, '2017-12-22 17:09:32', '2018-03-13 16:58:58'),
(30, 'google-plus', 'google-plus', '30.png', 1, 0, 4, '2017-12-22 17:09:47', '2018-03-13 16:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Password` char(32) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Photo` text NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Password`, `Role`, `Status`, `Photo`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(4, 'Admin', 'Admin', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 1, 1, '4.jpg', '2015-08-29 10:50:00', '2016-10-13 10:59:15', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bestseller`
--
ALTER TABLE `bestseller`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `gallerycat`
--
ALTER TABLE `gallerycat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallerylist`
--
ALTER TABLE `gallerylist`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `getacall`
--
ALTER TABLE `getacall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `getacallhome`
--
ALTER TABLE `getacallhome`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multimedia`
--
ALTER TABLE `multimedia`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `productcat`
--
ALTER TABLE `productcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sociallinks`
--
ALTER TABLE `sociallinks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bestseller`
--
ALTER TABLE `bestseller`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gallerycat`
--
ALTER TABLE `gallerycat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gallerylist`
--
ALTER TABLE `gallerylist`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `getacall`
--
ALTER TABLE `getacall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `getacallhome`
--
ALTER TABLE `getacallhome`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `multimedia`
--
ALTER TABLE `multimedia`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `productcat`
--
ALTER TABLE `productcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sociallinks`
--
ALTER TABLE `sociallinks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
