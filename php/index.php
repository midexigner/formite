<?php require_once 'admin/Common.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite  - Adding colors to your lifestyle</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <script src="assets/lib/angular.min.js"></script>
  <script src="assets/controllers.js"></script>
  <?php require_once 'include/css.php'; ?>

</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

require_once 'include/slider.php';

 ?>


   

<!-- // being about section -->
<section id="about">
	<div class="container">
		<div class="row">
      <?php 
      $query="SELECT ID,Heading,Image1,Text FROM about";
    $result = mysql_query ($query) or die(mysql_error()); 
    $num = mysql_num_rows($result); 
    $row=mysql_fetch_array($result);
    $image = $row["Image1"];?>
			<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
				<h2><?php echo $row['Heading'] ?></h2>
<?php echo $row['Text'] ?>
			</div>
			<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.5s">
        <div class="tc-image-effect-shine">
				<img src="<?php echo SITE_URL.'/admin/'.DIR_ABOUT.$image; ?>" alt="" class="img-responsive">
      </div>
			</div>
		</div>
	</div>
</section>
<!-- // end about section -->

<!-- // being product section -->
<section id="product">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Our Products</h2>
				<p>Discover our individuality by exploring our fine parameters of product category</p>
			</div>
		</div>

    <?php 
$productquery="SELECT content,slug,name,status,image AS Image
        FROM productcat WHERE Status = 1";
    $productresult = mysql_query ($productquery) or die(mysql_error()); 
    $gallerynum = mysql_num_rows($productresult);
$i = 0;
$j = 0;
while ($productrow=mysql_fetch_array($productresult)) {  $i++; $j++;    
 ?>
 <?php if ($j==1) { ?>
    <div class="row product-service wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="200ms">
  <?php } ?>
	
      <div class="col-md-4 col-sm-4 col-xs-4">
           <div class="tc-image-anim-zoom-out">
        <div class="products-list">
        <div class="shadow"></div>
				<img src="<?php echo SITE_URL.'/admin/'.DIR_PRODUCTS_CATEGORY.$productrow['Image']; ?>" class="img-responsive" alt="img-2">
				<div class="caption">
					<h3><a href="<?php echo SITE_URL.'/product/'.$productrow['slug']; ?>"><?php echo $productrow['name'] ?></a></h3>
					<p><a href="<?php echo SITE_URL.'/product/'.$productrow['slug']; ?>">Explore More</a></p>
				
</div>
			</div>
    </div>
  </div>
			
		
 <?php if ($j==3) { ?> </div> <?php $j=0; } ?> 
  
<?php } ?>

    
	</div>
</section>
<!-- // end product section -->
<!-- // being gallery section -->
<section id="gallery">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Our Gallery</h2>
				<p>Here you can observe all extensive variety of patterns and colors accessible to you</p>
			</div>
		</div>
    <?php 
    $galleryquery="SELECT content,slug,name,status,image AS Image
        FROM gallerycat WHERE status = 1";
    $galleryresult = mysql_query ($galleryquery) or die(mysql_error()); 
    $gallerynum = mysql_num_rows($galleryresult);
$i = 0;
$j = 0; 
while ($galleryrow=mysql_fetch_array($galleryresult)) {  $i++; $j++;    
 ?>
 <?php if ($j==1) { ?>
<div class="row gallery-service wow fadeInUp" data-wow-delay="200ms">
  <?php } ?>

        <div class="col-md-4 col-sm-4 col-xs-4">
        <div class="tc-image-anim-zoom-out">
      <div class="gallery-list ">
<div class="shadow"></div>
          <img src="<?php echo SITE_URL.'/admin/'.DIR_GALLERY_CATEGORY.$galleryrow['Image']; ?>" class="img-responsive" alt="img-4">
		<div class="caption"><h3><a href="<?php echo SITE_URL.'/gallery/'.$galleryrow['slug']; ?>"><?php echo $galleryrow['name'] ?></a></h3><p><a href="<?php echo SITE_URL.'/gallery/'.$galleryrow['slug']; ?>">Explore More</a></p></div>
				</div>
				</div>
      </div>
		
<?php if ($j==3) { ?> </div> <?php $j=0; } ?> 
  
<?php } ?>
	</div>
</section>
<!-- // end gallery section -->

<!-- // being best seller section -->
<section id="best-seller">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Best Sellers</h2>
				<p>Be astounded with our alluring range of best selling colors</p>
			</div>
		</div>
		<div class="row best-seller-service text-center  wow fadeInRight" data-wow-delay="200ms">

<div id="carousel-best-seller" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
$i = 0;
$query = "SELECT ID FROM bestseller WHERE Status = 1 ORDER BY Sort ASC";
$result = mysql_query($query) or die(mysql_error());
$num = mysql_num_rows($result);
while($row = mysql_fetch_array($result))
{
  $i++;
?>
    <li data-target="#carousel-best-seller" data-slide-to="<?php echo $i;?>" class="<?php echo ($i == 1) ? 'active' : ''; ?>"></li>
  <?php } ?>  
    
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php
$i = 0;
$query = "SELECT ID,Heading,Headingtwo,Headingthree,CornerTag,  RightImage,Slider FROM bestseller WHERE Status = 1 ORDER BY Sort ASC";
$result = mysql_query($query) or die(mysql_error());
$num = mysql_num_rows($result);
while($row = mysql_fetch_array($result))
{
$i++;
?>
    <div class="item <?php echo ($i == 1) ? 'active' : ''; ?>">
        <div class="best-seller-service-list "><div class="col-md-4 col-sm-4 col-xs-4">
        <div class="tc-image-effect-circle">
                <img src="<?php echo (is_file('admin/'.DIR_BEST_SELLER.dboutput($row["Slider"])) ? 'admin/'.DIR_BEST_SELLER.dboutput($row["Slider"]) : ''); ?>" class="img-responsive" alt="" />
                </div>
                <h3><?php echo $row["Heading"]; ?></h3>
                </div>
                </div>
      <div class="best-seller-service-list"><div class="col-md-4 col-sm-4 col-xs-4">
        <div class="tc-image-effect-circle">
                <img src="<?php echo (is_file('admin/'.DIR_BEST_SELLER.dboutput($row["CornerTag"])) ? 'admin/'.DIR_BEST_SELLER.dboutput($row["CornerTag"]) : ''); ?>" class="img-responsive" alt="" />
                 </div>
             <h3><?php echo $row["Headingtwo"]; ?></h3>
               
                </div></div>
      <div class="best-seller-service-list"><div class="col-md-4 col-sm-4 col-xs-4">
         <div class="tc-image-effect-circle">
                <img src="<?php echo (is_file('admin/'.DIR_BEST_SELLER.dboutput($row["RightImage"])) ? 'admin/'.DIR_BEST_SELLER.dboutput($row["RightImage"]) : ''); ?>" class="img-responsive" alt="" />
                </div>
               <h3><?php echo $row["Headingthree"]; ?></h3>
                </div></div>
    </div>
     <?php } ?>
    
  </div>

 
</div>

		
			
		</div>
	</div>
</section>
<!-- // best seller section -->
<!--// being free quote clients-->
    <section id="free-quote-clients">
	<div class="container">
		<div class="row free-quote-clients-service">
			<div class="free-quote-service-list" ng-app="sa_app" ng-controller="controller" ><div class="col-md-6   wow fadeInRight" data-wow-delay="200ms">
                <h3>Get a Call</h3>
                <p>Want us to give you a call? Fill the frame underneath to meet your prerequisites and spending plan</p>
                
                <form  name="getcallhomes" class="form-horizontal">
                   <div class="row">
                    
                     <div class="form-group col-md-6">
                    <input type="text" class="form-control" ng-model="name"  placeholder="Your name"/>
                    </div>
                     <div class="form-group col-md-6">
                    <input type="email" class="form-control" ng-model="email"  placeholder="Your email" />
                    </div>
                    </div>
                    
                    <div class="row">
                         <div class="form-group col-md-12">
                    <textarea class="form-control msg-textarea" ng-model="message" placeholder="Your message"></textarea>
                        </div>
                    </div>
                    
                      <div class="row">
                    
                     <div class="form-group col-md-3">
                    <input type="submit" class="form-control submit" ng-click="getcallhome()"  value="{{btnNames}}"/>
                    </div>
                    <div  ng-model="resultss" ng-show="resultss" class="alert alert-success">
                    {{resultss}}
                </div> 
                    
                    </div>
                
                </form>
                </div></div>
			<div class="clients-service-list"><div class="col-md-6   wow fadeInLeft" data-wow-delay="200ms">
               
                <h3>Our Clients</h3>
                <p>There are all awesome people</p>
                
                <div id="clients" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
     <!--  <ol class="carousel-indicators">
        <li data-target="#clients" data-slide-to="0" class="active"></li>
        <li data-target="#clients" data-slide-to="1"></li>
        <li data-target="#clients" data-slide-to="2"></li>
      </ol> -->
      <div class="carousel-inner" role="listbox">
           <?php
$i = 0;
$query = "SELECT ID,Heading,Text, Slider FROM testimonials WHERE Status = 1";
$result = mysql_query($query) or die(mysql_error());
$num = mysql_num_rows($result);
while($row = mysql_fetch_array($result))
{
//$row = mysql_fetch_array($result);
$i++;

?>

        <div class="item <?php echo ($i == 1) ? 'active' : ''; ?>">
         
            <div class="clients-caption">
             <blockquote>
                <?php echo dboutput($row["Text"]); ?></blockquote>
                <div class="author-name">
                <div class="pull-left author-text"><?php echo dboutput($row["Heading"]); ?></div>
                    <div class="pull-right author-img"><img src="<?php echo (is_file('admin/'.DIR_TESTIMONIALS.dboutput($row["Slider"])) ? 'admin/'.DIR_TESTIMONIALS.dboutput($row["Slider"]) : ''); ?>" class="img-responsive" /></div>
                </div>
            </div>
         
         
        </div>
  <?php } ?>         
<!--
            <div class="item">
         
            <div class="clients-caption">
            <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit nulla pariatur.</p></blockquote>
            </div>
         
         
        </div>
-->
        
      </div>
      <!--<a class="left carousel-control" href="#clients" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#clients" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>-->
    </div>
                </div></div>
			
			
		</div>
	</div>
</section>
<!--// end free quote clients-->

<!-- // being latest news -->
    <section id="latest-news">
    <div class="container">
      <?php 
$PID = 9;
 $pquery="SELECT ID,Heading,Tagline,Status,Image,Text
        FROM pages WHERE  ID = $PID  AND Status = 1 ORDER BY Sort ASC";
    $presult = mysql_query ($pquery) or die(mysql_error()); 
    $pnum = mysql_num_rows($presult);
$prow=mysql_fetch_array($presult);
   $image = $prow["Image"];

       ?>
        <div class="row text-center">
			<div class="heading">
				<h2><?php echo ucFirst(strtolower($prow['Tagline'])); ?></h2>
				<div class="col-md-8 col-md-offset-2">
                <?php echo $prow['Text']; ?>
                </div>
			</div>
		</div>
    <div class="row  wow fadeInUp" data-wow-delay="200ms">
      
      <?php 
$partnerquery="SELECT ID,Heading,Status,Slider AS Image,Text,Slug
        FROM partners WHERE Status = 1 ORDER BY Sort ASC";
    $partnerresult = mysql_query ($partnerquery) or die(mysql_error()); 
    $partnernum = mysql_num_rows($partnerresult);

while ($partnerrow=mysql_fetch_array($partnerresult)) {       ?>  
     <div class="col-xs-6 col-md-4 col-sm-4 mb20">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
          <a href="<?php echo SITE_URL.'/partner/'.$partnerrow['Slug']; ?>">
        <img src="<?php echo (is_file('admin/'.DIR_PARTNERS.dboutput($partnerrow["Image"])) ? 'admin/'.DIR_PARTNERS.dboutput($partnerrow["Image"]) : ''); ?>" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
 <?php } ?>       
    
        </div>   

      
    </div>
    </section>
<!-- // end latest news -->

<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>