<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$image; ?>" alt="gallery-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2><?php echo $row['Tagline']; ?></h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
 </div>

   
<!-- // being product section -->
<section id="gallery-content">
  <div class="container">
    <?php echo $row['Text']; ?>

<?php 
$galleryquery="SELECT content,slug,name,status,image AS Image
        FROM gallerycat WHERE Status = 1";
    $galleryresult = mysql_query ($galleryquery) or die(mysql_error()); 
    $gallerynum = mysql_num_rows($galleryresult);
$i = 0;
$j = 0;
while ($galleryrow=mysql_fetch_array($galleryresult)) {  $i++; $j++;    
 ?>
 <?php if ($j==1) { ?>
<div class="row gallery-service wow fadeInUp" data-wow-delay="200ms">
  <?php }  
?>
          <div class="col-md-4">
      <div class="gallery-list">
        <div class="tc-image-anim-zoom-out">
        <div class="shadow"></div>
       <a href="<?php echo SITE_URL.'/gallery/'.$galleryrow['slug']; ?>">

            <img src="<?php echo SITE_URL.'/admin/'.DIR_GALLERY_CATEGORY.$galleryrow['Image']; ?>" class="img-responsive" alt="img-4">
     </div>
    <div class="caption"><h3><?php echo $galleryrow['name']; ?></h3><p><?php echo $galleryrow['content']; ?></p></div>
        </div>
       </a>
        </div>
         <?php if ($j==3) { ?> </div> <?php $j=0; } ?> 
  
<?php } ?>

  </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>