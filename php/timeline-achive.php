<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
     <title>Formite</title>
  <link href="https://fonts.googleapis.com/css?family=Seaweed+Script" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
  <?php require_once 'include/css.php'; ?>



</head>
<body>

  <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header timeline-achive" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$image; ?>" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2><?php echo $row['Tagline']; ?></h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>


   
<!-- // being product section -->
<section id="content" class="introduction">
  <div class="container">
    <div class="row">
      
        <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
       <h3><?php echo $row['Heading']; ?></h3>
       <?php echo $row['Text']; ?>
       
 </div>
      
    </div>
  </div>
  
</section> 
<br>
<section class="white-bg" id="content-two">
  <div class="container">
    <div class="row">
      <?php 
      $query="SELECT ID,Heading,Text,content FROM timelineachive";
    $result = mysql_query ($query) or die(mysql_error()); 
    $num = mysql_num_rows($result); 
    $rowss=mysql_fetch_array($result);
    ?>
     <div class="col-md-4">
       <div id="formite-product-history">
         <h3><?php echo $rowss['Heading']; ?></h3>
         <?php echo $rowss['content']; ?>
<!-- <div class="media">
  <div class="media-left">
  
      <img src="assets/img/timeline-1.png" alt="timeline-1">
    
  </div>
  <div class="media-body">
    
    <p>A pioneer in the production of High Pressure Laminates, Formite was established in 1980. Refinement in the product quality, lead to the fructification of Melamite in 1983. Production of the first</p>
   
  </div>
   <p class="text-color"> laminated board was yet again another milestone attained in 1983.</p>
   <h5>We are in the business 
of adding 
<span  class="green">c</span>
<span  class="light-blue">o</span>
<span  class="red">l</span>
<span  class="dark-blue">o</span>
<span  class="orange">r</span>
<span  class="light-green">s</span>
to your
<span class="color-lifestyle"> 
lifestyle!</span></h5>
</div> -->
       </div>
      <div class="clearfix"></div>
       <div class="adsss">
           <?php
           $id = 5;
	 $query="SELECT ID,Heading, Image
        FROM banners WHERE  ID = '$id' AND Status = 1";
    $result = mysql_query ($query) or die(mysql_error()); 
    $num = mysql_num_rows($result);
    $rows=mysql_fetch_array($result);
           ?>
         <img src="<?php echo (is_file('admin/'.DIR_BANNERS.dboutput($rows["Image"])) ? 'admin/'.DIR_BANNERS.dboutput($rows["Image"]) : ''); ?>" alt="">
       </div>
     </div>
      <div class="col-md-8">
  <?php echo $rowss['Text']; ?>



        </div> 
    </div>
  </div>
</section> 

 <section id="content-one" class="introduction">
  <div class="container">
    <div class="row">
      
        <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
        <h3 class="text-center"style="font-family: 'Kaushan Script', cursive;text-transform: inherit;margin-bottom: 0px;line-height: 38px;font-size: 26px;">Make a statement of your powerful expression by letting Formite create a beautiful, functional and comfortable space that is a reflection of your personality!</h3>
        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p> -->
 </div>
      
    </div>
  </div>
  
</section> 
<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?>   

</body>
</html>