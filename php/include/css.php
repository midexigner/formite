	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/normalize.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/lightbox.min.css">
	
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/animate.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/font-awesome.min.css">
  	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/hoverme-effect.css">
	<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/style.css">

	<script src="<?php echo SITE_URL; ?>/assets/js/vendor/modernizr-2.8.3.min.js"></script>
 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->