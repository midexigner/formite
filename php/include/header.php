	<div id="socialmediaLink">
  <div class="socialHeader">
    <h3>follow our style journey</h3>
  </div>
 <main>
  <?php 
 $SelectSocialMediaquery="SELECT ID,Heading,Image,Text FROM sociallinks WHERE ID <>0";
    $SelectSocialMediaresult = mysql_query ($SelectSocialMediaquery) or die(mysql_error()); 
    $SelectSocialMedianum = mysql_num_rows($SelectSocialMediaresult);
   

   ?>
    <ul class="list-unstyled">
      <?php while($SelectSocialMediarow=mysql_fetch_array($SelectSocialMediaresult)){ ?>
    <li><a href="<?php echo SITE_URL.'/'.strtolower($SelectSocialMediarow['Text']); ?>"><img src="<?php echo SITE_URL.'/admin/'.DIR_SOCIALLINKS,'/'.$SelectSocialMediarow['Image']; ?>"></a></li>
<?php } ?>
  </ul>
 </main>
</div>

  <header id="header">
		 <div class="navbar-wrapper">
      <div class="container-fluid">
	<div class="row">
    
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container fluid-container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo SITE_URL; ?>"><img src="<?php echo SITE_URL; ?>/assets/img/logo.png" alt="logo" class="img-responsive"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "index.php"){echo "class='active'"; } ?>><a href="index">Home</a></li>
              <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a> 
                      
                        <ul class="dropdown-menu">
                           <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "mission.php"){echo "class='active'"; } ?>><a href="<?php echo SITE_URL; ?>/mission">Mission</a></li>
                            <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "introduction.php"){echo "class='active'"; } ?>><a href="<?php echo SITE_URL; ?>/introduction">Introduction</a></li>
                            <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "timeline-achive.php"){echo "class='active'"; } ?>><a href="<?php echo SITE_URL; ?>/timeline-achive">Timeline of Achievements </a></li>
                          
                        </ul>
                    </li>
               <li class="dropdown">
    <a id="drop1" role="button" href="<?php echo SITE_URL; ?>/products">Products <b class="caret"></b></a>
    <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop1">
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/product/high-pressure-laminates">High Pressure Laminates</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/product/low-pressure-laminates">Low Pressure Laminates</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/product/compact-laminates">Compact Laminates</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/product/technical-laminates">Technical Laminates</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/product/speciality-laminates">Speciality Laminates</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/product/applications">Applications</a></li>
    </ul>
  </li>
         <li class="dropdown">
    <a id="drop2" role="button" href="<?php echo SITE_URL; ?>/gallery">Gallery <b class="caret"></b></a>
    <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop2">
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery/marble">Marble</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery/metallica">Metallica</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery/woodgrain">Woodgrain</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery/solid-colors">Solid Colors</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery/patterns">Patterns</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery/custom-laminates">Custom Laminates</a></li>
      <li role="presentation"><a role="menuitem" href="<?php echo SITE_URL; ?>/gallery-view-all.php">View All</a></li>
    </ul>
  </li>
              <!--   <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "products.php"){echo "class='active'"; } ?>><a href="<?php echo SITE_URL; ?>/products"></a></li> -->
               <!--  <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "gallery.php"){echo "class='active'"; } ?>><a href="<?php echo SITE_URL; ?>/gallery">Gallery</a></li> -->
                 <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "partners.php"){echo "class='active'"; } ?>><a href="<?php echo SITE_URL; ?>/our-partners">Our Partners</a></li>
                <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "contact-us.php"){echo "class='active'"; } ?>><a href="contact-us">Contact</a></li>
                 <li class="visualizerlogo"><img src="<?php echo SITE_URL; ?>/assets/img/logo-visualizer.png" alt="" class="img-responsive"></li>
               
              </ul>
            </div>
          </div>
        </nav>
</div>
      </div>
    </div>
	</header>
