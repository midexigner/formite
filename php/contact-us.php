<?php require_once 'admin/Common.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
     <script src="assets/lib/angular.min.js"></script>
  <script src="assets/controllers.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato|Pacifico" rel="stylesheet">
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header contact" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$image; ?>" alt="<?php echo $row['Tagline']; ?>">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2><?php echo $row['Tagline']; ?></h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="content" class="contact-us">
  <div class="container">
    <div class="row">
      <h3 class="contact-title wow fadeInRight text-center" data-wow-duration="0.5s" data-wow-delay="0.6s"><?php echo $row['Heading']; ?></h3>            
      
 </div>

 <div class="row">
   <div class="col-md-5">
    <?php echo $row['Text']; ?>
    <!--  <h3>Get In Touch</h3>
     <p>Want us to give you a call? <br/><br/>
We will do our best to meet your prerequisites and spending plan </p>
     <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-envelope"></i>
    </a>
  </div>
  <div class="media-body  media-middle">
    <p><a href="mailto:info@formite.com.pk">info@formite.com.pk</a></p>
  </div>
</div>

  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">HEAD OFFICE</h4>
4th Floor, House of Habib, Plot-3, JCHS, Block 7/8, <br/>Shahra-e-Faisal, Karachi  - Pakistan.<br/>
Tel: +92 213 4545 195-6
  </div>
</div>

  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">FACTORY OUTLET I</h4>
Timber Market, Siddiq Wahab Road, Karachi  - Pakistan.<br/>
Tel: +92 213 2723 335 
  </div>
</div>

  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">FACTORY OUTLET II</h4>
    
House # G-370, Gali # 2, Sir Syed Ahmed Khan <br/>Road, Manzoor Colony, Mehmoodabad, <br/>Karachi  - Pakistan.
Tel: +92 213 5887 624
  </div>
</div>
  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">FORMITE DISPLAY CENTER</h4>
   
Shop #6 G3/1 Clifton Pride, Block 8, KDA <br/>Scheme 5, Clifton, Karachi  - Pakistan.<br/>
Tel: +92 213 5865 601-2

  </div>
</div> -->

 
 <div class="social-media-contact">
  <?php 
   $SocialMediaquery="SELECT ID,Heading,Image,Text FROM sociallinks WHERE ID <>0";
    $SocialMediaresult = mysql_query ($SocialMediaquery) or die(mysql_error()); 
    $SocialMedianum = mysql_num_rows($SocialMediaresult); ?>
   <div class="ul list-inline">
     <?php while($SelectSocialMediarow=mysql_fetch_array($SelectSocialMediaresult)){ ?>
     <li><a href="<?php echo SITE_URL.'/'.strtolower($SelectSocialMediarow['Text']); ?>"><i class="fa fa-<?php echo strtolower($SelectSocialMediarow['Text']); ?>"></i></a></li>
     <?php } ?>
 <!--     <li><a href="#"><i class="fa fa-twitter"></i></a></li>
     <li><a href="#"><i class="fa fa-instagram"></i></a></li>
     <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
   </div>
 </div>

  
   </div>
   <div class="col-md-7"   ng-app="sa_app" ng-controller="controller">
     <div class="form-contact-bg">
      <div class="form-get-cell">
        <h4 class="title">Get a Call</h4>
       <form  role="form" name="myForm"  class="form-horizontal" >
          <div class="form-group">
            <input type="text" name="name" ng-model="name" id="name" placeholder="Your Name" class="form-control">
          </div>

          <div class="form-group">
            <input type="text" name="phone" ng-model="phone" id="phone" placeholder="Your Phone No." class="form-control">
          </div>
          <div class="form-group">
            <select name="interest" id="interest"   class="form-control" ng-model="interest"  >
              <option value="Interested In" selected="selected">Interested In</option>
               <option value="HIGH PRESSURE LAMINATES">HIGH PRESSURE LAMINATES </option>
                <option value="LOW PRESSURE LAMINATES">LOW PRESSURE LAMINATES</option>
                 <option value="COMPACT LAMINATES">COMPACT LAMINATES</option>
                 <option value="TECHNICAL LAMINATES">TECHNICAL LAMINATES</option>
                 <option value="SPECIALITY LAMINATES">SPECIALITY LAMINATES</option>
                 <option value="APPLICATION">APPLICATION</option>
                 
            </select>
          </div>
           <div class="form-group text-center">
            <input type="submit" value="{{btnName}}" ng-click="send()" class="btn btn-default">
          </div>
          <div  ng-model="result" ng-show="result" class="alert alert-success">
                    {{result}}
                </div>
       </form>
     </div>

      <div class="form-write-us">
        <h4 class="title">Write to Us</h4>
       <form  name="contactus" class="form-horizontal" >
          <div class="form-group">
            <input type="text" ng-model="yourname" id="your-name" placeholder="Your Name" class="form-control">
          </div>

          <div class="form-group">
            <input type="email" ng-model="email" id="email" placeholder="Your E-mail" class="form-control">
          </div>
          <div class="form-group">
            <input type="text" ng-model="company" id="company" placeholder="Company" class="form-control">
          </div>
           <div class="form-group">
            <input type="text" ng-model="phone" id="phone" placeholder="Contact No." class="form-control">
          </div>
         
           <div class="form-group">
           <textarea class="form-control" ng-model="message" id="message" placeholder="Message" rows="3"></textarea>
          </div>
          
           <div class="form-group text-center">
            <input type="submit"  value="{{btnName}}" ng-click="contact()" class="btn btn-default">
          </div>
          <div  ng-model="results" ng-show="results" class="alert alert-success">
                    {{results}}
                </div> 
       </form>
     </div>

     </div>

   </div>
 </div>
      
    </div>


  </div>
	
</section>



<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>