<?php include("Common.php"); ?>
<?php include("CheckAdminLogin.php"); ?>
<?php 
// if (!file_exists('assets/ordersdocuments/1')) {
    // mkdir('assets/ordersdocuments/1', 0755, true);
// }

$Today=date("Y-m-d");
$d=strtotime("-1 Days");		
$Yesterday=date("Y-m-d", $d);
$d=strtotime("-2 Days");		
$days2Date=date("Y-m-d", $d);
$days2Day=date("l", $d);
$d=strtotime("-3 Days");		
$days3Date=date("Y-m-d", $d);
$days3Day=date("l", $d);
$d=strtotime("-4 Days");		
$days4Date=date("Y-m-d", $d);
$days4Day=date("l", $d);
$d=strtotime("-5 Days");		
$days5Date=date("Y-m-d", $d);
$days5Day=date("l", $d);
$d=strtotime("-6 Days");		
$days6Date=date("Y-m-d", $d);
$days6Day=date("l", $d);
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
		<?php include_once("Sidebar.php"); ?>
		
		
		<?php include_once("Header.php"); ?>

       

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
		  
		
          </div>
        </div>
        <!-- /page content -->

		<?php include_once("Footer.php"); ?>
        
      </div>
    </div>

   <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- ECharts -->
    <script src="vendors/echarts/dist/echarts.min.js"></script>
    <script src="vendors/echarts/map/js/world.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
  </body>
</html>