<?php
include_once("Common.php");
include("CheckAdminLogin.php");

$ID=0;
$msg="";
$Heading="";
$Text="";
$Description="";
$Status=1;
$ShowOnHome=0;
$Sort=0;
$action = "";
$ID=0;

		foreach($_REQUEST as $key => $val)
			$$key = $val;
if($action == "delete")
{
	if(isset($_REQUEST["ids"]) && is_array($_REQUEST["ids"]))
	{
		foreach($_REQUEST["ids"] as $BID)
		{
			mysql_query("DELETE FROM careers_details  WHERE ID IN (" . $BID . ")") or die (mysql_error());
		}
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Delete All selected applications.</b>
		</div>';
		redirect("ViewCareer.php?ID=".(int)$_REQUEST["ID"]);
	
	}
	else
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please select career to delete.</b>
		</div>';
	}
}
foreach($_REQUEST as $key => $val)
	$$key = $val;
	
if(isset($_POST["action"]) && $_POST["action"] == "submit_form")
{			
	foreach($_POST as $key => $val)
		$$key = $val;

	if($Heading == "")
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please enter Heading.</b>
		</div>';
	}
	else if($Text == "")
	{
		$msg='<div class="alert alert-danger alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Please enter text.</b>
		</div>';
	}

	if($msg=="")
	{

		$query="UPDATE careers SET DateModified=NOW(),
				Heading = '" . dbinput($Heading) . "',
				Text = '" . dbinput($Text) . "',
				Status='".(int)$Status . "',
				Description='".dbinput($Description) . "',
				PerformedBy = '" . dbinput($_SESSION['UserID']) . "' Where ID = ".$ID."";
		mysql_query($query) or die (mysql_error());
		// echo $query;
		//$ID = mysql_insert_id();
		$_SESSION["msg"]='<div class="alert alert-success alert-dismissable">
		<i class="fa fa-ban"></i>
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<b>Career has been Updated.</b>
		</div>';
		redirect($_SERVER["PHP_SELF"].'?ID='.$ID);	
	}
}
else
{
	$query="SELECT * FROM careers WHERE  ID='" . (int)$ID . "'";
	$result = mysql_query ($query) or die(mysql_error()); 
	$num = mysql_num_rows($result);
	if($num==0)
	{
		$_SESSION["msg"]='<div class="alert alert-danger alert-dismissable">
			<i class="fa fa-ban"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Invalid Career ID.</b>
		</div>';
		redirect("Careers.php");
	}
	else
	{
		$row = mysql_fetch_array($result,MYSQL_ASSOC);
		
		$ID=$row["ID"];
		$Heading=$row["Heading"];
		$Text=$row["Text"];
		$Status=$row["Status"];
		$ShowOnHome=$row["ShowOnHome"];
		$Description=$row["Description"];
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>View Career</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php include_once("Sidebar.php"); ?>

        <?php include_once("Header.php"); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>View Career</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
						  <button class="btn btn-default" type="button">Go!</button>
					  </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Form</h2>
                    <ul class="nav navbar-right panel_toolbox">
					  <li><a href="Careers.php" class="btn btn-default active"><i class="fa fa-arrow-left"></i> Back</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
				  <?php
		  		echo $msg;
				if(isset($_SESSION["msg"]))
				{
					echo $_SESSION["msg"];
					$_SESSION["msg"]="";
				}
				?>
                  <div class="x_content">

                    <form id="frmPages" action="<?php echo $_SERVER["PHP_SELF"];?>?ID=<?php echo $ID; ?>" method="post"  enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate>
					  <input type="hidden" name="action" value="submit_form" />
					  <input type="hidden" name="ID" value="<?php echo $ID; ?>" />
                      <span class="section">Fill All Mandatory Fields</span>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Heading">Heading 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <p id="Heading" class="col-md-7 col-xs-12" ><?php echo dboutput($Heading); ?></p>
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Text">Text 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <p id="Text" class="col-md-7 col-xs-12" ><?php echo dboutput($Text); ?></p>
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Description">Description
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <p id="Description" class="col-md-7 col-xs-12" ><?php echo dboutput($Description); ?></p>
                        </div>
                      </div>
					  <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:8px;">
							<label>
							  <p id="Description" class="col-md-7 col-xs-12" ><?php echo ($Status == '1' ? 'Active' : ''); ?></p>
							</label>
							<label>
							  <p id="Description" class="col-md-7 col-xs-12" ><?php echo ($Status == '0' ? 'Deactive' : ''); ?></p>
							</label>
						</div>
					  </div>
                      <div class="ln_solid"></div>
					  <div class="text-right" ><a onClick="javascript:doDelete()" class="btn btn-danger active"><i class="fa fa-trash"></i> Bulk Delete</a></div>
		<?php 
		$query="SELECT *, DATE_FORMAT(DOB, '%d %m %Y') AS DOB, DATE_FORMAT(DateAdded, '%d %m %Y<br/>%r') AS DateAdded FROM careers_details WHERE  JobID = ".(int)$row["ID"];
		$result = mysql_query ($query) or die(mysql_error()); 
		$num = mysql_num_rows($result);
		?>
					<form id="frmPages" action="<?php echo $self;?>" class="form-horizontal no-margin" method="post">
					<input type="hidden" id="action" name="action" value="" />
					<input type="hidden" id="DeleteID" name="DeleteID" value="" />
                    <table class="table table-bordered table-striped sorted_table">
					  <thead>
					  <tr>
                        <th><input type="checkbox" name="chkAll" class="checkUncheckAll"></td>
                        <th>Personal Details</th>
                        <th>Education</th>
                        <th>Experience</th>
                        <th>Applied on</th>
					  </tr>
					  </thead>
					  <tbody>
<?php while($row=mysql_fetch_array($result))
{
	?>
                      <tr>
                        <td>
						<input type="hidden"name="item[]" value="<?php echo $row["ID"]; ?>" />
						<input type="checkbox" value="<?php echo $row["ID"]; ?>" name="ids[]" class="no-margin chkIds"></td>
                        <td>
							<b><?php echo dboutput($row["FullName"]); ?></b><br/>
							<b>Father Name:</b> <?php echo dboutput($row["FatherName"]); ?><br/>
							<b>NIC:</b> <?php echo dboutput($row["NIC"]); ?><br/>
							<b>Date of Birth:</b> <?php echo dboutput($row["DOB"]); ?><br/>
							<b>Marital Status:</b> <?php echo dboutput($row["MaritalStatus"]); ?><br/>
							<b>Contact Number:</b> <?php echo dboutput($row["Number"]); ?><br/>
							<b>Email:</b> <?php echo dboutput($row["Email"]); ?><br/>&nbsp;
						</td>
                        <td>
						<?php 
						$q1="SELECT * FROM careers_details_education WHERE ID <>0 AND JobDetailID = ".(int)$row["ID"];
						$r1 = mysql_query ($q1) or die(mysql_error()); 
						while($row2 = mysql_fetch_array($r1))
						{
						?>
						<b>Degree:</b> <?php echo dboutput($row2["Degree"]); ?><br/>
						<b>Passing Year:</b> <?php echo dboutput($row2["PassingYear"]); ?><br/>
						<b>Institute:</b> <?php echo dboutput($row2["Institute"]); ?><br/>
						<?php
						}
						?>
						</td>
                        <td>
						<?php 
						$q1="SELECT *, DATE_FORMAT(DateStart, '%d %m %Y') AS DateStart, DATE_FORMAT(DateEnd, '%d %m %Y') AS DateEnd FROM careers_details_job WHERE ID <>0 AND JobDetailID = ".(int)$row["ID"];
						$r1 = mysql_query ($q1) or die(mysql_error()); 
						while($row2 = mysql_fetch_array($r1))
						{
						?>
						<b>Company:</b> <?php echo dboutput($row2["Company"]); ?><br/>
						<b>Position:</b> <?php echo dboutput($row2["Position"]); ?><br/>
						<b>Salary:</b> <?php echo dboutput($row2["Salary"]); ?><br/>
						<b>From:</b> <?php echo dboutput($row2["DateStart"]); ?><br/>
						<b>To:</b> <?php echo dboutput($row2["DateEnd"]); ?><br/>
						<?php
						}
						?>
						</td>
						<td class="noPrint">
							<?php echo $row["DateAdded"]; ?>
						</td>
					  </tr>
<?php }
	?>
					  </tbody>
					</table>
					</form>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <?php include_once("Footer.php"); ?>
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>

    <!-- validator -->
    <script>
	$(document).ready(function () {				
		$(".checkUncheckAll").click(function () {
			$(".chkIds").prop("checked", $(this).prop("checked"));			
		});
	});
	var counter = 0;

	function doDelete()
	{
		if($(".chkIds").is(":checked"))
		{
			if(confirm("Are you sure to delete."))
			{
				$("#action").val("delete");
				$("#frmPages").submit();
			}
		}
		else
			alert("Please select User to delete");
	}
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgpreview')
                    .attr('src', e.target.result)
                    .height(142);
            };
            reader.readAsDataURL(input.files[0]);
        }
		else
                $('#imgpreview').attr('src', '').width(0).height(0);
    }
      // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
      });
    </script>
    <!-- /validator -->
  </body>
</html>