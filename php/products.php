<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php echo SITE_URL.'/admin/'.DIR_PAGES.$image; ?>" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2><?php echo $row['Tagline']; ?></h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->

</div>
   
<!-- // being product section -->
<section id="products-content">
  <div class="container">
    <div class="row">
      <?php echo $row['Text']; ?>

   
      <?php 
$galleryquery="SELECT content,slug,name,status,image AS Image
        FROM productcat WHERE Status = 1";
    $galleryresult = mysql_query ($galleryquery) or die(mysql_error()); 
    $gallerynum = mysql_num_rows($galleryresult);
$i = 0;
$j = 0;
while ($galleryrow=mysql_fetch_array($galleryresult)) {  $i++; $j++;    
 ?>
 <?php if ($j==1) { ?>
 <div class="row product-section wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
  <?php } ?>
  
      <div class="col-md-4">
        <div class="products-inner text-center">
           <div class="subheading">
          <a href="<?php echo SITE_URL.'/product/'.$galleryrow['slug']; ?>"><h3><?php echo $galleryrow['name'] ?>
            
          </h3></a>
        </div>
        <div class="figcaption">
           <div class="tc-image-anim-zoom-out">
          <img src="<?php echo SITE_URL.'/admin/'.DIR_PRODUCTS_CATEGORY.$galleryrow['Image']; ?>" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <?=strlen($galleryrow['content']) > 115 ? substr($galleryrow['content'],0,115)."..." : $galleryrow['content'];?>
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="<?php echo SITE_URL.'/product/'.$galleryrow['slug']; ?>" class="more-info">explore</a>
            </div>
            </div>
          </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
        <?php if ($j==3) { ?> </div> <?php $j=0; } ?> 
  
<?php } ?>
      </div>
    </div> 


    <!-- <div class="row product-section wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.8s">
      <div class="col-md-4">
        <div class="products-inner text-center">
           <div class="subheading">
          <a href="technical-laminates"><h3>Technical Laminates</h3></a>
        </div>
        <div class="figcaption">
          <img src="assets/img/product-img-004.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Grade P1 – The paper based P1 (PFCP 201) has mechanical applications and mechanical properties far better than other PFCP types ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="technical-laminates" class="more-info">explore</a>
            </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
          <a href="speciality-laminates"><h3>Speciality Laminates</h3></a>
          </div>
          <div class="figcaption">
          <img src="assets/img/product-img-005.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Fibrite – Synthetic Resin Bonded Fabric Laminate, also acknowledged as ‘FIBRITE’ has its application for both mechanical and ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="speciality-laminates" class="more-info">explore</a>
            </div>
            </div>
          </div>
        </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
           <a href="applications"> <h3>Application</h3></a>
         </div>
         <div class="figcaption">
          <img src="assets/img/product-img-006.jpg" alt="thumbnail" class="img-responsive">
            <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Coming Soon ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="applications" class="more-info">explore</a>
            </div>
          </div>
          </div>
          </div>
        
        </div>
      </div>
    </div> -->



  </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>