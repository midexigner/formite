<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Solid Colors</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp inner-gallery">
<div class="innerPorduct">
  <div class="container">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
 <li role="presentation"><a href="new-finishes">New Finishes</a></li>
  <li role="presentation"><a href="marble">Marble</a></li>
  <li role="presentation"><a href="#">Metallica</a></li>
  <li role="presentation"><a href="woodgrain">WoodGrain</a></li>
 <li role="presentation" class="active"><a href="plain-colors">Solid Colors</a></li>
 <li role="presentation"><a href="patterns">Patterns</a></li>
 <li role="presentation"><a href="custom-laminates">Custom Laminates</a></li> 
</ul>
  <br>
  <br>
  <br>
      </div>
   <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>


</div>

<div class="col-md-9">
  <div class="products-inner-content">
    <div class="bg-1">
   <h3>Solid Colors</h3>
 <p>Delve into our ravishing selection of solid color range.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/8903.jpg" alt="8903">
    </div>
      <div class="caption">
        <p>Color Code <span>8903</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/8903.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7014.jpg" alt="7014">
    </div>
      <div class="caption">
        <p>Color Code <span>7014</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7014.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7015.jpg" alt="7015">
    </div>
      <div class="caption">
        <p>Color Code <span>7015</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7015.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7017.jpg" alt="7017">
    </div>
      <div class="caption">
        <p>Color Code <span>7017</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7017.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7019.jpg" alt="7019">
    </div>
      <div class="caption">
        <p>Color Code <span>7019</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7019.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7021.jpg" alt="7021">
    </div>
      <div class="caption">
        <p>Color Code <span>7021</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7021.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7022.jpg" alt="7022">
    </div>
      <div class="caption">
        <p>Color Code <span>7022</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7022.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7023.jpg" alt="7023">
    </div>
      <div class="caption">
        <p>Color Code <span>7023</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7023.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7024.jpg" alt="7024">
    </div>
      <div class="caption">
        <p>Color Code <span>7024</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7024.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7043.jpg" alt="7043">
    </div>
      <div class="caption">
        <p>Color Code <span>7043</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7043.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7046.jpg" alt="7046">
    </div>
      <div class="caption">
        <p>Color Code <span>7046</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7046.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7047.jpg" alt="7047">
    </div>
      <div class="caption">
        <p>Color Code <span>7047</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7047.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7052.jpg" alt="7052">
    </div>
      <div class="caption">
        <p>Color Code <span>7052</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7052.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7053.jpg" alt="7053">
    </div>
      <div class="caption">
        <p>Color Code <span>7053</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7053.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7054.jpg" alt="7054">
    </div>
      <div class="caption">
        <p>Color Code <span>7054</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7054.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7056.jpg" alt="7056">
    </div>
      <div class="caption">
        <p>Color Code <span>7056</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7056.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7072.jpg" alt="7072">
    </div>
      <div class="caption">
        <p>Color Code <span>7072</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7072.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7073.jpg" alt="7073">
    </div>
      <div class="caption">
        <p>Color Code <span>7073</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7073.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7076.jpg" alt="7076">
    </div>
      <div class="caption">
        <p>Color Code <span>7076</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7076.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7077.jpg" alt="7077">
    </div>
      <div class="caption">
        <p>Color Code <span>7077</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7077.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7100.jpg" alt="7100">
    </div>
      <div class="caption">
        <p>Color Code <span>7100</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7100.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7600.jpg" alt="7600">
    </div>
      <div class="caption">
        <p>Color Code <span>7600</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7600.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/7601.jpg" alt="7601">
    </div>
      <div class="caption">
        <p>Color Code <span>7601</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7601.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/plain/8507.jpg" alt="8507">
    </div>
      <div class="caption">
        <p>Color Code <span>8507</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/8507.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>



</div>

</div>


  


    </div>




    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>