<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
     <title>Formite</title>
  <link href="https://fonts.googleapis.com/css?family=Seaweed+Script" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
  <?php require_once 'include/css.php'; ?>



</head>
<body>

  <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header timeline-achive" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/timeline-banner.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Timeline of Achievements</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>


   
<!-- // being product section -->
<section id="content" class="introduction">
  <div class="container">
    <div class="row">
      
        <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
       <h3>FROM EMERGENCE TO EXCELLENCE</h3>
        <p>Since 1980, Fomite, a brand of Baluchistan Laminates is proud to give you all it takes to make your interiors wow. As the largest manufacturer and supplier of decorative & technical laminates in Pakistan, our tradition to transform ambiances continues. Year after year, we deliver on the same promise, and here we live it again. This is to challenging your imagination. This is to making homes and offices, spaces and buildings speak loud and beyond. Let’s do it together.
</p>
 </div>
      
    </div>
  </div>
  
</section> 
<br>
<section class="white-bg" id="content-two">
  <div class="container">
    <div class="row">
     <div class="col-md-4">
       <div id="formite-product-history">
         <h3>FORMITE
Product History</h3>
<div class="media">
  <div class="media-left">
  
      <img src="assets/img/timeline-1.png" alt="timeline-1">
    
  </div>
  <div class="media-body">
    
    <p>A pioneer in the production of High Pressure Laminates, Formite was established in 1980. Refinement in the product quality, lead to the fructification of Melamite in 1983. Production of the first</p>
   
  </div>
   <p class="text-color"> laminated board was yet again another milestone attained in 1983.</p>
   <h5>We are in the business 
of adding 
<span  class="green">c</span>
<span  class="light-blue">o</span>
<span  class="red">l</span>
<span  class="dark-blue">o</span>
<span  class="orange">r</span>
<span  class="light-green">s</span>
to your
<span class="color-lifestyle"> 
lifestyle!</span></h5>
</div>
       </div>
      <div class="clearfix"></div>
       <div class="adsss">
         <img src="assets/img/timeline-2.jpg" alt="">
       </div>
     </div>
      <div class="col-md-8">
  <div id="timeline-achive">
    <div class="row text-center">
   <p style="font-family: 'Seaweed Script', cursive;
    color: #e01f27;
    font-size: 29px;
    line-height: 35px;"">Innovation 
Continues...
</p>
</div>
<div class="row">
  <div class="col-md-6 border-right"><br>
        <br>
        <br><br></div>
  <div class="col-md-6"></div>
</div>
    <div class="row">


    <div class="col-md-6 border-right">
     <div class="content text-right">
        <div class="timeline-badge"></div>
        <img src="assets/img/logo-1.jpg" class="wow zoomIn" alt="" data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
    <div class="col-md-6 text-left ">
      <div class="timeline-year-1 wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-1.png" alt="">
      </div>
    </div>
  </div>
   <div class="row">
  
    <div class="col-md-6 border-right">
      <div class="timeline-badge  badge-two"></div>
      <div class="timeline-year-2 text-right wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-6.png" alt="">
      </div>
    </div>
      <div class="col-md-6">
     <div class="content text-left">
        <img src="assets/img/logo-6.png" alt="" class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
  </div>

   <div class="row">
    <div class="col-md-6 border-right">
     <div class="content text-right">
        <div class="timeline-badge"></div>
        <img src="assets/img/logo-7.png" class="wow zoomIn" alt="" data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
    <div class="col-md-6 text-left ">
      <div class="timeline-year-1 wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-7.png" alt="">
      </div>
    </div>
  </div>

    <div class="row">
  
    <div class="col-md-6 border-right">
      <div class="timeline-badge  badge-two"></div>
      <div class="timeline-year-2 text-right wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-8.png" alt="">
      </div>
    </div>
      <div class="col-md-6">
     <div class="content text-left">
        <img src="assets/img/logo-8.png" alt="" class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
  </div>

   <div class="row">
    <div class="col-md-6 border-right">
     <div class="content text-right">
        <div class="timeline-badge"></div>
        <img src="assets/img/logo-9.png" class="wow zoomIn" alt="" data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
    <div class="col-md-6 text-left ">
      <div class="timeline-year-1 wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-9.png" alt="">
      </div>
    </div>
  </div>

    <div class="row">
  
    <div class="col-md-6 border-right">
      <div class="timeline-badge  badge-two"></div>
      <div class="timeline-year-2 text-right wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-2.png" alt="">
      </div>
    </div>
      <div class="col-md-6">
     <div class="content text-left">
        <img src="assets/img/logo-2.png" alt="" class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 border-right">
      <div class="timeline-badge badge-two"></div>
     <div class="content text-right">
        <img src="assets/img/logo-3.png" alt="" class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
    <div class="col-md-6">
      <div class="timeline-year-3 text-left wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-3.png" alt="">
      </div>
    </div>
  </div>

 <div class="row">
  
    <div class="col-md-6 border-right">
      <div class="timeline-badge badge-four"></div>
      <div class="timeline-year-4 text-right wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-4.png" alt="">
      </div>
    </div>
      <div class="col-md-6">
     <div class="content text-left">
        <img src="assets/img/logo-4.png" alt="" class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
     </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 border-right">
      <div class="timeline-badge badge-five"></div>
     <div class="content text-right height-none">
        <img src="assets/img/logo-5.png" alt="" class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
        
     </div>
    </div>
    <div class="col-md-6">

      <div class="timeline-year-5 text-left wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
      <img src="assets/img/year-5.png" alt="">
      </div>
    </div>
  </div>



  </div>




        </div> 
    </div>
  </div>
</section> 

 <section id="content-one" class="introduction">
  <div class="container">
    <div class="row">
      
        <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
        <h3 class="text-center"style="font-family: 'Kaushan Script', cursive;text-transform: inherit;margin-bottom: 0px;line-height: 38px;font-size: 26px;">Make a statement of your powerful expression by letting Formite create a beautiful, functional and comfortable space that is a reflection of your personality!</h3>
        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p> -->
 </div>
      
    </div>
  </div>
  
</section> 
<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?>   

</body>
</html>