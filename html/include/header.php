	<div id="socialmediaLink">
  <div class="socialHeader">
    <h3>follow our style journey</h3>
  </div>
 <main>
    <ul class="list-unstyled">
    <li><a href=""><img src="assets/img/social/icon-facebook.png"></a></li>
    <li><a href=""><img src="assets/img/social/icon-twitter.png"></a></li>
    <li><a href=""><img src="assets/img/social/icon-instagram.png"></a></li>
    <li><a href=""><img src="assets/img/social/icon-google-plus.png"></a></li>

  </ul>
 </main>
</div>

  <header id="header">
		 <div class="navbar-wrapper">
      <div class="container-fluid">
	<div class="row">
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php"><img src="assets/img/logo.png" alt="logo"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "index.php"){echo "class='active'"; } ?>><a href="index">Home</a></li>
              <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a> 
                      
                        <ul class="dropdown-menu">
                           <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "mission.php"){echo "class='active'"; } ?>><a href="mission">Mission</a></li>
                            <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "introduction.php"){echo "class='active'"; } ?>><a href="introduction">Introduction</a></li>
                            <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "timeline-achive.php"){echo "class='active'"; } ?>><a href="timeline-achive">Timeline of Achievements </a></li>
                          
                        </ul>
                    </li>
               <!--  <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "about-us.php"){echo "class='active'"; } ?>><a href="javascript:avoid(0)">About</a></li> -->
                <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "products.php"){echo "class='active'"; } ?>><a href="products">Products</a></li>
                <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "gallery.php"){echo "class='active'"; } ?>><a href="gallery">Gallery</a></li>
                 <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "partners.php"){echo "class='active'"; } ?>><a href="partners">Our Partners</a></li>
                <li <?php if(basename($_SERVER['SCRIPT_NAME']) == "contact-us.php"){echo "class='active'"; } ?>><a href="contact-us">Contact</a></li>
                 <li class="visualizerlogo"><img src="assets/img/logo-visualizer.png" alt=""></li>
               
              </ul>
            </div>
          </div>
        </nav>
</div>
      </div>
    </div>
	</header>
