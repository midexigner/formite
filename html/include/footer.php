<!--// being footer -->
    <footer class="footer">
    <div class="container">
<div class="row">
<div class="footer-list">
<div class="col-md-4">
<h3>Uniqueness in Exclusivity</h3>
<p>A pioneer and market leader in the laminates industry, Formite provides its client an interior, that is unique and exclusive, to surpass their expectations by having acquired state of the art technology and defined efficient planning processes. </p>
<h4>HEAD OFFICE</h4>
    <p>4th Floor, House of Habib, Plot-3, JCHS, Block 7/8,  Shahra-e-Faisal, Karachi, Pakistan</p>
<p>E-mail: info@formite.com.pk <br/>
Phone: +92 213 4545 195-6</p>
</div>
    </div> 
    <div class="footer-list">
<div class="col-md-4">

<h3>More Links</h3>
    <ul class="list-unstyle footer-link">
      <li><a href="index">Home</a></li>
       <li><a href="mission">Mission</a></li>
        <li><a href="introduction">Introduction</a></li>
        <li><a href="timeline-achive">Timeline of Achievements </a></li>
       <li><a href="products">Products</a></li>
       <li><a href="gallery">Gallery</a></li>
       <li><a href="partners">Our Partners</a></li>
       <li><a href="contact-us">Contact</a></li>
    </ul>
</div>
    </div> 
    
    <div class="footer-list">
<div class="col-md-4">
<h3>Search</h3>
    <form class="form-inline">
      <div class="form-group">
        <label for="search" class="sr-only">Search</label>
        <input type="search" class="form-control" id="search" placeholder="Type your keyword ...">
      </div>
      <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div id="scroll-to"></div>
</div>
    </div> 
</div> 

        <div class="row text-center copyright">
        <p>Copyrights 2017 <a href="index.php">Formite</a>. All rights reserved. Designed &amp; Developed By <a href="cloud-innovator.com">Cloud Innovators Solution</a></p>
        </div>
</div>
    </footer>
<!--// end footer -->