<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Lato" rel="stylesheet">
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

require_once 'include/slider.php';

 ?>


   

<!-- // being about section -->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
				<h2>Welcome to Formite</h2>
<p class="red">Magnificent Craftiness  </p>
				
				<p>Having evolved as a market leader in laminating industry, Formite is highly focused towards constructing durable and exquisite surroundings that enhance the customer’s standard of living. 
</p><p>
Our eye-catching array of product designs are premeditated to make certain that our clientele is delivered the promised lifestyle they deserve.
</p>
			</div>
			<div class="col-md-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.5s">
        <div class="tc-image-effect-shine">
				<img src="assets/img/img-1.jpg" alt="" class="img-responsive">
      </div>
			</div>
		</div>
	</div>
</section>
<!-- // end about section -->

<!-- // being product section -->
<section id="product">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Our Products</h2>
				<p>Discover our individuality by exploring our fine parameters of product category</p>
			</div>
		</div>
		<div class="row product-service wow fadeInUp" data-wow-delay="200ms">
      <div class="col-md-4">
				<div class="products-list">
				<img src="assets/img/product-img-001.jpg" class="img-responsive" alt="img-2">
				<div class="caption">
					<h3><a href="high-pressure-laminates">High Pressure Laminates</a></h3>
					<p><a href="high-pressure-laminates">Explore More</a></p>
				
</div>
			</div>
    </div>
			<div class="col-md-4">
        
				<div class="products-list">
					<img src="assets/img/product-img-2.jpg" class="img-responsive" alt="img-3">
				<div class="caption">
					<h3><a href="low-pressure-laminates">LOW Pressure Laminates</a></h3>
					<p><a href="low-pressure-laminates">Explore More</a></p>
				</div>
			
    </div>
			</div>
			<div class="col-md-4">
	<div class="products-list">
			<img src="assets/img/product-img-6.jpg" class="img-responsive" alt="img-4">
<div class="caption">
					<h3><a href="compact-laminates">Compact Laminates</a></h3>
					<p><a href="compact-laminates">Explore More</a></p>
				</div>
			</div>
    </div>
		
		</div>

      <div class="row product-service wow fadeInUp last" data-wow-delay="200ms">
      <div class="col-md-4">
        <div class="products-list">
        <img src="assets/img/product-img-004.jpg" class="img-responsive" alt="img-2">
        <div class="caption">
          <h3><a href="technical-laminates">TECHNICAL Laminates</a></h3>
          <p><a href="technical-laminates">Explore More</a></p>
        
</div>
      </div>
    </div>
      <div class="col-md-4">
        
        <div class="products-list">
          <img src="assets/img/product-img-005.jpg" class="img-responsive" alt="img-3">
        <div class="caption">
          <h3><a href="speciality-laminates">Speciality Laminates</a></h3>
          <p><a href="speciality-laminates">Explore More</a></p>
        </div>
      
    </div>
      </div>
      <div class="col-md-4">
  <div class="products-list">
      <img src="assets/img/product-img-006.jpg" class="img-responsive" alt="img-4">
<div class="caption">
          <h3><a href="applications">Application</a></h3>
          <p><a href="applications">Explore More</a></p>
        </div>
      </div>
    </div>
    
    </div>
	</div>
</section>
<!-- // end product section -->
<!-- // being gallery section -->
<section id="gallery">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Our Gallery</h2>
				<p>Here you can observe all extensive variety of patterns and colors accessible to you</p>
			</div>
		</div>
		<div class="row gallery-service wow fadeInDown" data-wow-delay="200ms">
			<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-1.jpg" class="img-responsive" alt="img-4">
		<div class="caption"><h3><a href="marble">Marble</a></h3><p><a href="marble">Explore More</a></p></div>
				</div>
				</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-2.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3><a href="#">Metallica</a></h3>
  <p><a href="#">Explore More</a></p></div>
				</div>
					</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-3.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3><a href="woodgrain">Woodgrain</a></h3><p><a href="woodgrain">Explore More</a></p></div>
				</div>
			</div>
		</div>

		<div class="row gallery-service wow fadeInUp" data-wow-delay="200ms">
			<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-4.jpg" class="img-responsive" alt="img-4">
		<div class="caption"><h3><a href="plain-colors">Solid Colors</a></h3><p><a href="plain-colors">Explore More</a></p></div>
				</div>
				</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-5.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3><a href="patterns">Patterns</a></h3><p><a href="patterns">Explore More</a></p></div>
				</div>
					</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-6.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3><a href="custom-laminates">Custom Laminates</a></h3><p><a href="custom-laminates">Explore More</a></p></div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- // end gallery section -->

<!-- // being best seller section -->
<section id="best-seller">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Best Sellers</h2>
				<p>Be astounded with our alluring range of best selling colors</p>
			</div>
		</div>
		<div class="row best-seller-service text-center  wow fadeInRight" data-wow-delay="200ms">

<div id="carousel-best-seller" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-best-seller" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-best-seller" data-slide-to="1"></li>
    <li data-target="#carousel-best-seller" data-slide-to="2"></li>
    <li data-target="#carousel-best-seller" data-slide-to="3"></li>
    <li data-target="#carousel-best-seller" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <div class="best-seller-service-list "><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-1.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Marble - 8106 </h3>
                </div>
                </div>
      <div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-2.jpg" class="img-responsive" alt="" />
                 </div>
               <h3>Solid - 7072 </h3>
               
                </div></div>
      <div class="best-seller-service-list"><div class="col-md-4">
         <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-3.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Pattern - 8727 </h3>
                </div></div>
    </div>
    <div class="item">
      <div class="best-seller-service-list "><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-4.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Woodgrain - 7124 </h3>
                </div>
                </div>
      <div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-5.jpg" class="img-responsive" alt="" />
                 </div>
               <h3>Solid - 7047 </h3>
               
                </div></div>
      <div class="best-seller-service-list"><div class="col-md-4">
         <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-6.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Pattern - 8626 </h3>
                </div></div>
    </div>

    <div class="item">
        <div class="best-seller-service-list "><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-7.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Woodgrain - 7196 </h3>
                </div>
                </div>
      <div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-8.jpg" class="img-responsive" alt="" />
                 </div>
               <h3>Marble - 8145 </h3>
               
                </div></div>
      <div class="best-seller-service-list"><div class="col-md-4">
         <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-9.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Solid - 7076 </h3>
                </div></div>
    </div>

       <div class="item">
        <div class="best-seller-service-list "><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-10.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Marble - 8144 </h3>
                </div>
                </div>
      <div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-11.jpg" class="img-responsive" alt="" />
                 </div>
               <h3>Pattern - 8446 </h3>
               
                </div></div>
      <div class="best-seller-service-list"><div class="col-md-4">
         <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-12.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Woodgrain - 7415 </h3>
                </div></div>
    </div>

     <div class="item">
        <div class="best-seller-service-list "><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-13.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Pattern - 8478 </h3>
                </div>
                </div>
      <div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-14.jpg" class="img-responsive" alt="" />
                 </div>
               <h3>Plain - 7045 </h3>
               
                </div></div>
      <div class="best-seller-service-list"><div class="col-md-4">
         <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-15.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Woodgrain - 7413 </h3>
                </div></div>
    </div>
    
  </div>

 
</div>

		
			
		</div>
	</div>
</section>
<!-- // best seller section -->
<!--// being free quote clients-->
    <section id="free-quote-clients">
	<div class="container">
		<div class="row free-quote-clients-service">
			<div class="free-quote-service-list"><div class="col-md-6   wow fadeInRight" data-wow-delay="200ms">
                <h3>Get a Call</h3>
                <p>Want us to give you a call? Fill the frame underneath to meet your prerequisites and spending plan</p>
                
                <form class="form-horizontal">
                   <div class="row">
                    
                     <div class="form-group col-md-6">
                    <input type="text" class="form-control"  placeholder="Your name"/>
                    </div>
                     <div class="form-group col-md-6">
                    <input type="email" class="form-control"  placeholder="Your email" />
                    </div>
                    </div>
                    
                    <div class="row">
                         <div class="form-group col-md-12">
                    <textarea class="form-control msg-textarea" placeholder="Your message"></textarea>
                        </div>
                    </div>
                    
                      <div class="row">
                    
                     <div class="form-group col-md-3">
                    <input type="submit" class="form-control submit"  value="Submit"/>
                    </div>
                    
                    </div>
                
                </form>
                </div></div>
			<div class="clients-service-list"><div class="col-md-6   wow fadeInLeft" data-wow-delay="200ms">
               
                <h3>Our Clients</h3>
                <p>There are all awesome people</p>
                
                <div id="clients" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
     <!--  <ol class="carousel-indicators">
        <li data-target="#clients" data-slide-to="0" class="active"></li>
        <li data-target="#clients" data-slide-to="1"></li>
        <li data-target="#clients" data-slide-to="2"></li>
      </ol> -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
         
            <div class="clients-caption">
             <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit nulla pariatur.</p></blockquote>
                <div class="author-name">
                <div class="pull-left author-text">Ali Vohra</div>
                    <div class="pull-right author-img"><img src="assets/img/client-img-1.png" class="img-responsive" /></div>
                </div>
            </div>
         
         
        </div>
          
<!--
            <div class="item">
         
            <div class="clients-caption">
            <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit nulla pariatur.</p></blockquote>
            </div>
         
         
        </div>
-->
        
      </div>
      <!--<a class="left carousel-control" href="#clients" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#clients" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>-->
    </div>
                </div></div>
			
			
		</div>
	</div>
</section>
<!--// end free quote clients-->

<!-- // being latest news -->
    <section id="latest-news">
    <div class="container">
        <div class="row text-center">
			<div class="heading">
				<h2>Our Partners</h2>
				<div class="col-md-8 col-md-offset-2">
                <p>Formite partners with the leading brands to craft intricate interplays of innovative solutions that surpass client’s expectation </p>
                </div>
			</div>
		</div>
    <div class="row  wow fadeInUp   mb20" data-wow-delay="200ms">
        
     <div class="col-sm-6 col-md-4 ">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
          <a href="">
        <img src="assets/img/partner-1.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
        <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
          <a href="">
        <img src="assets/img/partner-2.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
     <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
          <a href="">
        <img src="assets/img/partner-3.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
    
        </div>   

         <div class="row  wow fadeInUp " data-wow-delay="200ms">
        
     <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
       <a href="">
          <img src="assets/img/partner-4.png" class="img-responsive" alt="latest-news-img-1">
       </a>
     </div>
       </div>
    </div>
        
        <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
          <a href="">
        <img src="assets/img/partner-5.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
     <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out h-165">
          <a href="">
        <img src="assets/img/partner-6.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
    
        </div>  
    </div>
    </section>
<!-- // end latest news -->

<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>