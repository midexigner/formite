<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Pacifico" rel="stylesheet">
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header contact" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/contact-banner.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Contact Us</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="content" class="contact-us">
  <div class="container">
    <div class="row">
      <h3 class="contact-title wow fadeInRight text-center" data-wow-duration="0.5s" data-wow-delay="0.6s">We’d love to hear from you!</h3>            
      
 </div>

 <div class="row">
   <div class="col-md-5">
     <h3>Get In Touch</h3>
     <p>Want us to give you a call? <br/><br/>
We will do our best to meet your prerequisites and spending plan </p>
     <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-envelope"></i>
    </a>
  </div>
  <div class="media-body  media-middle">
    <p><a href="mailto:info@formite.com.pk">info@formite.com.pk</a></p>
  </div>
</div>

  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">HEAD OFFICE</h4>
4th Floor, House of Habib, Plot-3, JCHS, Block 7/8, <br/>Shahra-e-Faisal, Karachi  - Pakistan.<br/>
Tel: +92 213 4545 195-6
  </div>
</div>

  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">FACTORY OUTLET I</h4>
Timber Market, Siddiq Wahab Road, Karachi  - Pakistan.<br/>
Tel: +92 213 2723 335 
  </div>
</div>

  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">FACTORY OUTLET II</h4>
    
House # G-370, Gali # 2, Sir Syed Ahmed Khan <br/>Road, Manzoor Colony, Mehmoodabad, <br/>Karachi  - Pakistan.
Tel: +92 213 5887 624
  </div>
</div>
  <div class="media">
  <div class="media-left">
    <a href="#">
      <i class="fa fa-map-marker"></i>
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">FORMITE DISPLAY CENTER</h4>
   
Shop #6 G3/1 Clifton Pride, Block 8, KDA <br/>Scheme 5, Clifton, Karachi  - Pakistan.<br/>
Tel: +92 213 5865 601-2

  </div>
</div>

 
 <div class="social-media-contact">
   <div class="ul list-inline">
     <li><a href="#"><i class="fa fa-facebook"></i></a></li>
     <li><a href="#"><i class="fa fa-twitter"></i></a></li>
     <li><a href="#"><i class="fa fa-instagram"></i></a></li>
     <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
   </div>
 </div>

  
   </div>
   <div class="col-md-7">
     <div class="form-contact-bg">
      <div class="form-get-cell">
        <h4 class="title">Get a Call</h4>
       <form action="" class="form-horizontal" method="post">
          <div class="form-group">
            <input type="text" name="name" id="name" placeholder="Your Name" class="form-control">
          </div>

          <div class="form-group">
            <input type="text" name="phone" id="phone" placeholder="Your Phone No." class="form-control">
          </div>
          <div class="form-group">
            <select name="interest" id="interest"  class="form-control" placeholder="Your Phone No.">
              <option value="interestedin">Interested In</option>
               <option value="interestedin">HIGH PRESSURE LAMINATES </option>
                <option value="interestedin">LOW PRESSURE LAMINATES</option>
                 <option value="interestedin">COMPACT LAMINATES</option>
                 <option value="interestedin">TECHNICAL LAMINATES</option>
                 <option value="interestedin">SPECIALITY LAMINATES</option>
                 <option value="interestedin">APPLICATION</option>
                 
            </select>
          </div>
           <div class="form-group text-center">
            <input type="submit" value="send" class="btn btn-default">
          </div>
       </form>
     </div>

      <div class="form-write-us">
        <h4 class="title">Write to Us</h4>
       <form action="" class="form-horizontal" method="post">
          <div class="form-group">
            <input type="text" name="your-name" id="your-name" placeholder="Your Name" class="form-control">
          </div>

          <div class="form-group">
            <input type="email" name="email" id="email" placeholder="Your E-mail" class="form-control">
          </div>
          <div class="form-group">
            <input type="text" name="company" id="company" placeholder="Company" class="form-control">
          </div>
           <div class="form-group">
            <input type="text" name="company" id="company" placeholder="Contact No." class="form-control">
          </div>
           <div class="form-group">
            <input type="text" name="company" id="company" placeholder="Company" class="form-control">
          </div>
           <div class="form-group">
           <textarea class="form-control" name="message" id="message" placeholder="Message" rows="3"></textarea>
          </div>
          
           <div class="form-group text-center">
            <input type="submit" value="send" class="btn btn-default">
          </div>
       </form>
     </div>

     </div>

   </div>
 </div>
      
    </div>


  </div>
	
</section>



<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>