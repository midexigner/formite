<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

require_once 'include/slider.php';

 ?>


   

<!-- // being about section -->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
				<h2>Welcome to Formite</h2>
				<p class="red">We make it a priority to offer flexible services to accommodate your needs</p>
				<p>Being a market leader in lamination industry, Formite is highly focused towards constructing durable and exquisite surroundings that enhance the customer’s standard of living.</p>
				<p>For the first time in Pakistan, from the conception of an idea till its completion, we offer custom lamination services to ensure that customers are delivered the lifestyle they are promised.</p>
			</div>
			<div class="col-md-6 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.5s">
        <div class="tc-image-effect-shine">
				<img src="assets/img/img-1.jpg" alt="" class="img-responsive">
      </div>
			</div>
		</div>
	</div>
</section>
<!-- // end about section -->

<!-- // being product section -->
<section id="product">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Our Products</h2>
				<p>Explore our product category of high quality laminates.</p>
			</div>
		</div>
		<div class="row product-service wow fadeInUp" data-wow-delay="200ms">
      <div class="col-md-4">
				<div class="products-list">
				<img src="assets/img/product-img-1.jpg" class="img-responsive" alt="img-2">
				<div class="caption">
					<h3>High Pressure Laminates</h3>
					<p><a href="">Explore More</a></p>
				
</div>
			</div>
    </div>
			<div class="col-md-4">
        
				<div class="products-list">
					<img src="assets/img/product-img-2.jpg" class="img-responsive" alt="img-3">
				<div class="caption">
					<h3>LOW Pressure Laminates</h3>
					<p><a href="">Explore More</a></p>
				</div>
			
    </div>
			</div>
			<div class="col-md-4">
	<div class="products-list">
			<img src="assets/img/product-img-3.jpg" class="img-responsive" alt="img-4">
<div class="caption">
					<h3>Compact Laminates</h3>
					<p><a href="">Explore More</a></p>
				</div>
			</div>
    </div>
		
		</div>

      <div class="row product-service wow fadeInUp" data-wow-delay="200ms">
      <div class="col-md-4">
        <div class="products-list">
        <img src="assets/img/product-img-1.jpg" class="img-responsive" alt="img-2">
        <div class="caption">
          <h3>Tecnical Laminates</h3>
          <p><a href="">Explore More</a></p>
        
</div>
      </div>
    </div>
      <div class="col-md-4">
        
        <div class="products-list">
          <img src="assets/img/product-img-2.jpg" class="img-responsive" alt="img-3">
        <div class="caption">
          <h3>Sepcialty Laminates</h3>
          <p><a href="">Explore More</a></p>
        </div>
      
    </div>
      </div>
      <div class="col-md-4">
  <div class="products-list">
      <img src="assets/img/product-img-3.jpg" class="img-responsive" alt="img-4">
<div class="caption">
          <h3>Application</h3>
          <p><a href="">Explore More</a></p>
        </div>
      </div>
    </div>
    
    </div>
	</div>
</section>
<!-- // end product section -->
<!-- // being gallery section -->
<section id="gallery">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Our Gallery</h2>
				<p>Here you can observe all extensive variety of patterns and colors accessible to you.</p>
			</div>
		</div>
		<div class="row gallery-service wow fadeInDown" data-wow-delay="200ms">
			<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-1.jpg" class="img-responsive" alt="img-4">
		<div class="caption"><h3>Marble</h3><p>Design.Interior.home</p></div>
				</div>
				</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-2.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>Metallica</h3><p>Design.Interior.home</p></div>
				</div>
					</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-3.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>Woodgrain</h3><p>Design.Interior.home</p></div>
				</div>
			</div>
		</div>

		<div class="row gallery-service wow fadeInUp" data-wow-delay="200ms">
			<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-1.jpg" class="img-responsive" alt="img-4">
		<div class="caption"><h3>Plain Colors</h3><p>Design.Interior.home</p></div>
				</div>
				</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-2.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>Patterns</h3><p>Design.Interior.home</p></div>
				</div>
					</div>
				<div class="gallery-list">
				<div class="col-md-4"><img src="assets/img/gallery-img-3.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>Custom Laminates</h3><p>Design.Interior.home</p></div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- // end gallery section -->

<!-- // being best seller section -->
<section id="best-seller">
	<div class="container">
		<div class="row text-center">
			<div class="heading">
				<h2>Best Sellers</h2>
				<p>Explore our major projects that have done successfully with remarkable</p>
			</div>
		</div>
		<div class="row best-seller-service text-center  wow fadeInRight" data-wow-delay="200ms">
			<div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-1.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Daylight Interior and flooring </h3>
                </div>
                </div>
			<div class="best-seller-service-list"><div class="col-md-4">
        <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-2.jpg" class="img-responsive" alt="" />
                 </div>
                <h3>Daylight Interior and flooring </h3>
               
                </div></div>
			<div class="best-seller-service-list"><div class="col-md-4">
         <div class="tc-image-effect-circle">
                <img src="assets/img/best-seller-img-3.jpg" class="img-responsive" alt="" />
                </div>
                <h3>Daylight Interior and flooring </h3>
                </div></div>
			
		</div>
	</div>
</section>
<!-- // best seller section -->
<!--// being free quote clients-->
    <section id="free-quote-clients">
	<div class="container">
		<div class="row free-quote-clients-service">
			<div class="free-quote-service-list"><div class="col-md-6   wow fadeInRight" data-wow-delay="200ms">
                <h3>Get a Call</h3>
                <p>For a free quote, fill the frame underneath. We will do our best to meet your prerequisites and spending plan.</p>
                
                <form class="form-horizontal">
                   <div class="row">
                    
                     <div class="form-group col-md-6">
                    <input type="text" class="form-control"  placeholder="Your name"/>
                    </div>
                     <div class="form-group col-md-6">
                    <input type="email" class="form-control"  placeholder="Your email" />
                    </div>
                    </div>
                    
                    <div class="row">
                         <div class="form-group col-md-12">
                    <textarea class="form-control msg-textarea" placeholder="Your message"></textarea>
                        </div>
                    </div>
                    
                      <div class="row">
                    
                     <div class="form-group col-md-3">
                    <input type="submit" class="form-control submit"  value="Submit"/>
                    </div>
                    
                    </div>
                
                </form>
                </div></div>
			<div class="clients-service-list"><div class="col-md-6   wow fadeInLeft" data-wow-delay="200ms">
               
                <h3>Our Clients</h3>
                <p>There are all awesome people</p>
                
                <div id="clients" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
     <!--  <ol class="carousel-indicators">
        <li data-target="#clients" data-slide-to="0" class="active"></li>
        <li data-target="#clients" data-slide-to="1"></li>
        <li data-target="#clients" data-slide-to="2"></li>
      </ol> -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
         
            <div class="clients-caption">
             <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit nulla pariatur.</p></blockquote>
                <div class="author-name">
                <div class="pull-left author-text">Ali Vohra</div>
                    <div class="pull-right author-img"><img src="assets/img/client-img-1.png" class="img-responsive" /></div>
                </div>
            </div>
         
         
        </div>
          
<!--
            <div class="item">
         
            <div class="clients-caption">
            <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit nulla pariatur.</p></blockquote>
            </div>
         
         
        </div>
-->
        
      </div>
      <!--<a class="left carousel-control" href="#clients" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#clients" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>-->
    </div>
                </div></div>
			
			
		</div>
	</div>
</section>
<!--// end free quote clients-->

<!-- // being latest news -->
    <section id="latest-news">
    <div class="container">
        <div class="row text-center">
			<div class="heading">
				<h2>Get Inspired</h2>
				<div class="col-md-8 col-md-offset-2">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                </div>
			</div>
		</div>
    <div class="row  wow fadeInUp" data-wow-delay="200ms">
        
     <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img src="assets/img/latest-news-img-1.jpg" class="img-responsive" alt="latest-news-img-1">
        <div class="caption">
          <h3>Lorem ipsum dolor sit amet,</h3>
            <div class="author">
                <p>by: <span class="red">Formite</span> 28 Aug 2017   <span class="red">0 comment</span></p>
            </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <p class="text-right"><a href="#" class="btn btn-link" role="button">Read More <i class="fa fa-long-arrow-right"></i></a></p> 
        </div>
      </div>
    </div>
        
            <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img src="assets/img/latest-news-img-2.jpg" class="img-responsive" alt="latest-news-img-2">
        <div class="caption">
          <h3>Lorem ipsum dolor sit amet,</h3>
            <div class="author">
                <p>by: <span class="red">Formite</span> 28 Aug 2017   <span class="red">0 comment</span></p>
            </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <p class="text-right"><a href="#" class="btn btn-link" role="button">Read More <i class="fa fa-long-arrow-right"></i></a></p> 
        </div>
      </div>
    </div>
        
            <div class="col-sm-6 col-md-4">
      <div class="thumbnail">
        <img src="assets/img/latest-news-img-3.jpg" class="img-responsive" alt="latest-news-img-3">
        <div class="caption">
          <h3>Lorem ipsum dolor sit amet,</h3>
            <div class="author">
                <p>by: <span class="red">Formite</span> 28 Aug 2017   <span class="red">0 comment</span></p>
            </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <p class="text-right"><a href="#" class="btn btn-link" role="button">Read More <i class="fa fa-long-arrow-right"></i></a></p> 
        </div>
      </div>
    </div>
        
        
        
        </div>    
    </div>
    </section>
<!-- // end latest news -->

<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>