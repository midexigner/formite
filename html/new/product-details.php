<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->


   
<!-- // being product section -->
<section id="products-content" class="innerp">
<div class="innerPorduct">
  <div class="container mb68">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation" class="active"><a href="#">High Pressure Laminates</a></li>
  <li role="presentation"><a href="#">Low Pressure Laminates</a></li>
  <li role="presentation"><a href="#">Compact Laminates</a></li>

 <li role="presentation"><a href="#">Technical Laminates</a></li>
 <li role="presentation"><a href="#">Specialty Laminates</a></li>
 <li role="presentation"><a href="#">Applications</a></li> 
</ul>
      </div>
  


</div>

<div class="col-md-7">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>High Pressure Laminates</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive" alt="">
 </div>
 </div>


</div>

</div>


  


    </div>





  <div class="section-white">
    <div class="container">
        <div class="row">
        <div class="col-md-3">
    <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="" class="img-responsive">
    </div>
     <div class="ads">
      <img src="assets/img/product-inner-ads-2.jpg" alt="" class="img-responsive">
    </div>

   </div>

   <div class="products-inner-content">
     <div class="col-md-7">


 <div class="content-inner">
  <p>
   HPL is produced by saturating multiple layers of kraft paper with phenolic resin. A layer of printed décor paper is placed on top of the kraft paper before pressing. The resulting sandwich is fused together under heat and pressure (more than 1,000 PSI). The combination of heat and pressure creates a single, rigid laminated sheet. Thermosetting creates strong, irreversible bonds that contribute to HPL’s durability.
 </p>
 <img src="assets/img/product-inner-img-2.jpg" class="img-responsive img" alt="">

 <p>HPL is laminated to a panel utilizing a variety of adhesives. Particleboard or MDF are the preferred substrate because they provide a stable, durable, consistent and economical foundation. Due to its durability,</p>

 <img src="assets/img/product-inner-ads-3.jpg" alt="" class="img-responsive">
 <p>HPL is a common choice for horizontal surfaces including flooring, countertops and desktops. It also performs well in horizontal and vertical applications for high traffic settings such as hospitality, office furniture, healthcare, retail casework, commercial interiors and educational facilities</p>
</div>
      </div>
   </div>


    </div>
      <div class="row"><p>The typical thicknesses available are 0.45mm, 0.6mm and 0.8mm with a standard size of 1220mm x 2440mm (8 x 4 feet).</p></div>
        <div class="row listingproductQuality">
    <div class="col-md-3">
      <div class="list-group">
   <div class="list-group-item active">
    Name
  </div>
  <div class="list-group-item">
    Cras justo odio
  </div>
   <div class="list-group-item">
    Cras justo odio
  </div>
   <div class="list-group-item">
    Cras justo odio
  </div>
   <div class="list-group-item">
    Cras justo odio
  </div>
</div>
    </div>
     <div class="col-md-3">
      <div class="list-group">
  <div  class="list-group-item active">
   Thickness
  </div>
   <div class="list-group-item">
    0.45 mm ± 0.05mm
  </div>
   <div class="list-group-item">
    0.60 mm ± 0.05mm
  </div>
   <div class="list-group-item">
   0.80 mm ± 0.05mm
  </div>
   <div class="list-group-item">
   0.45 mm ± 0.05mm
  </div>
</div>
    </div>
     <div class="col-md-3">
      <div class="list-group">
  <div class="list-group-item active">
  1220 mm x 2440 mm
  </div>
   <div class="list-group-item">
   1220 mm x 2440 mm
  </div>
   <div class="list-group-item">
    1220 mm x 2440 mm
  </div>
   <div class="list-group-item">
   1220 mm x 2440 mm
  </div>
   <div class="list-group-item">
   1220 mm x 2440 mm
  </div>
</div>
    </div>
    <div class="col-md-3">
      <div class="list-group">
 <div class="list-group-item active">
  Dull, Shine, Texture
  </div>
   <div class="list-group-item">
   Dull, Shine, Texture, Ribwood, Super Matt, Canvas
  </div>
   <div class="list-group-item">
    Dull, Shine, Texture, Ribwood, Super Matt, Canvas
  </div>
   <div class="list-group-item">
   Dull, Shine, Texture, Ribwood, Super Matt, Canvas
  </div>
   <div class="list-group-item">
   Dull, Shine, Texture, Ribwood, Super Matt, Canvas
  </div>
</div>
    </div>
  
  </div>
    </div>
  </div>


    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>