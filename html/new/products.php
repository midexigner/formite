<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->


   
<!-- // being product section -->
<section id="products-content">
  <div class="container">
    <div class="row">
      <div class="heading wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.6s">
        <h3>Formite Deals in a Range of Pressurize Laminations</h3>
        <p>Being a market leader in lamination industry, Formite is highly focused towards constructing durable and exquisite surroundings that enhance the customer’s standard of living.  For the first time in Pakistan, from the conception of an idea till its completion, we offer custom lamination services to ensure that customers are delivered the lifestyle they are promised.</p>
      </div>
    </div>

    <div class="row product-section wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.7s">
      <div class="col-md-4">
        <div class="products-inner text-center">
           <div class="subheading">
          <a href=""><h3>High Pressure Laminates</h3></a>
        </div>
        <div class="figcaption">
          <img src="assets/img/product-img-4.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>HPL is considered to be one of the most durable decorative surface materials and is 
available with special performance ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
          <a href=""><h3>LOW Pressure Laminates</h3></a>
          </div>
          <div class="figcaption">
          <img src="assets/img/product-img-5.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>HPL is considered to be one of the most durable decorative surface materials and is 
available with special performance ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
           <a href=""><h3>Compact Laminates</h3></a>
         </div>
         <div class="figcaption">
          <img src="assets/img/product-img-6.jpg" alt="thumbnail" class="img-responsive">
            <div class="caption">
            <div class="caption-heading visible-lg">
              <p>HPL is considered to be one of the most durable decorative surface materials and is 
available with special performance ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="" class="more-info">explore</a>
            </div>
          </div><!-- end caption -->
          </div>
          </div>
        
        </div>
      </div>
    </div> 

    <div class="row product-section wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.8s">
      <div class="col-md-4">
        <div class="products-inner text-center">
           <div class="subheading">
          <a href="#"><h3>Tecnical Laminates</h3></a>
        </div>
        <div class="figcaption">
          <img src="assets/img/product-img-4.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>HPL is considered to be one of the most durable decorative surface materials and is 
available with special performance ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="#" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
          <a href="#"><h3>Sepcialty Laminates</h3></a>
          </div>
          <div class="figcaption">
          <img src="assets/img/product-img-5.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>HPL is considered to be one of the most durable decorative surface materials and is 
available with special performance ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="#" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
           <a href=""> <h3>Application</h3></a>
         </div>
         <div class="figcaption">
          <img src="assets/img/product-img-6.jpg" alt="thumbnail" class="img-responsive">
            <div class="caption">
            <div class="caption-heading visible-lg">
              <p>HPL is considered to be one of the most durable decorative surface materials and is 
available with special performance ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="#" class="more-info">explore</a>
            </div>
          </div><!-- end caption -->
          </div>
          </div>
        
        </div>
      </div>
    </div>



  </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>