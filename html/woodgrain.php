<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>WoodGrain</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->

</div>
   
<!-- // being product section -->
<section id="products-content" class="innerp inner-gallery">
<div class="innerPorduct">
  <div class="container">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
 <li role="presentation"><a href="new-finishes">New Finishes</a></li>
  <li role="presentation"><a href="marble">Marble</a></li>
  <li role="presentation"><a href="#">Metallica</a></li>
  <li role="presentation" class="active"><a href="woodgrain">WoodGrain</a></li>
 <li role="presentation"><a href="plain-colors">Solid Colors</a></li>
 <li role="presentation"><a href="patterns">Patterns</a></li>
 <li role="presentation"><a href="custom-laminates">Custom Laminates</a></li> 
</ul>
  <br>
  <br>
  <br>
      </div>
   <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>


</div>

<div class="col-md-9">
  <div class="products-inner-content">
    <div class="bg-1">
   <h3>WoodGrain</h3>
 <p>Explore a perfectly appealing range of Wood grain colors and surfaces.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/2060.jpg" alt="2060">
    </div>
      <div class="caption">
        <p>Color Code <span>2060</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/2060.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/2088.jpg" alt="2088">
    </div>
      <div class="caption">
        <p>Color Code <span>2088</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/2088.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7102.jpg" alt="7102">
    </div>
      <div class="caption">
        <p>Color Code <span>7102</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7102.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7116.jpg" alt="7116">
    </div>
      <div class="caption">
        <p>Color Code <span>7116</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7116.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7124.jpg" alt="7124">
    </div>
      <div class="caption">
        <p>Color Code <span>7124</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7124.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7155.jpg" alt="7155">
    </div>
      <div class="caption">
        <p>Color Code <span>7155</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7155.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7168.jpg" alt="7168">
    </div>
      <div class="caption">
        <p>Color Code <span>7168</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7168.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7180.jpg" alt="7180">
    </div>
      <div class="caption">
        <p>Color Code <span>7180</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7180.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7182.jpg" alt="7182">
    </div>
      <div class="caption">
        <p>Color Code <span>7182</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7182.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7184.jpg" alt="7184">
    </div>
      <div class="caption">
        <p>Color Code <span>7184</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7184.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7196.jpg" alt="7196">
    </div>
      <div class="caption">
        <p>Color Code <span>7196</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7196.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7198.jpg" alt="7198">
    </div>
      <div class="caption">
        <p>Color Code <span>7198</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7198.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7241.jpg" alt="7241">
    </div>
      <div class="caption">
        <p>Color Code <span>7241</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7241.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7352.jpg" alt="7352">
    </div>
      <div class="caption">
        <p>Color Code <span>7352</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7352.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7357.jpg" alt="7357">
    </div>
      <div class="caption">
        <p>Color Code <span>7357</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7357.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7361.jpg" alt="7361">
    </div>
      <div class="caption">
        <p>Color Code <span>7361</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7361.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7363.jpg" alt="7363">
    </div>
      <div class="caption">
        <p>Color Code <span>7363</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7363.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7401.jpg" alt="7401">
    </div>
      <div class="caption">
        <p>Color Code <span>7401</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7401.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7405.jpg" alt="7405">
    </div>
      <div class="caption">
        <p>Color Code <span>7405</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7405.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7406.jpg" alt="7406">
    </div>
      <div class="caption">
        <p>Color Code <span>7406</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7406.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7407.jpg" alt="7407">
    </div>
      <div class="caption">
        <p>Color Code <span>7407</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7407.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7413.jpg" alt="7413">
    </div>
      <div class="caption">
        <p>Color Code <span>7413</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7413.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7415.jpg" alt="7415">
    </div>
      <div class="caption">
        <p>Color Code <span>7415</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7415.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7417.jpg" alt="7417">
    </div>
      <div class="caption">
        <p>Color Code <span>7417</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7417.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7418.jpg" alt="7418">
    </div>
      <div class="caption">
        <p>Color Code <span>7418</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7418.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7419.jpg" alt="7419">
    </div>
      <div class="caption">
        <p>Color Code <span>7419</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7419.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7423.jpg" alt="7423">
    </div>
      <div class="caption">
        <p>Color Code <span>7423</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7423.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7424.jpg" alt="7424">
    </div>
      <div class="caption">
        <p>Color Code <span>7424</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7424.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7486.jpg" alt="7486">
    </div>
      <div class="caption">
        <p>Color Code <span>7486</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7486.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail"> <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7495.jpg" alt="7495">
    </div>
      <div class="caption">
        <p>Color Code <span>7495</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7495.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/woodgrain/7591.jpg" alt="7591">
    </div>
      <div class="caption">
        <p>Color Code <span>7591</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7591.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 
 


 </div>


</div>

</div>


  


    </div>




    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>