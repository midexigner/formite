<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp">
<div class="innerPorduct">
  <div class="container mb68">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked tabs">
  <li role="presentation" class="active">
    <a href=".high-pressure-laminates" data-tab="high-pressure-laminates" role="tab" data-toggle="tab">High Pressure Laminates</a>
  </li>
  <li role="presentation">
    <a href=".low-pressure-laminates" data-tab="low-pressure-laminates" role="tab" data-toggle="tab">Low Pressure Laminates</a>
 </li>
  <li role="presentation">
<a href=".compact-laminates" data-tab="compact-laminates" role="tab" data-toggle="tab">Compact Laminates</a>
</li>

 <li role="presentation">
<a href=".technical-laminates" data-tab="technical-laminates" role="tab" data-toggle="tab">Technical Laminates</a>
</li>
 <li role="presentation">
  <a href=".speciality-laminates" data-tab="specialty-laminate" role="tab" data-toggle="tab">Speciality Laminates</a>
 </li>
 <li role="presentation">
 <a href=".applications" data-tab="applications" role="tab" data-toggle="tab">Applications</a>
 </li> 
</ul>
      </div>
  


</div>

<div class="col-md-9">
<div class="tab-content">
   <div  class="tab-pane fade in active high-pressure-laminates">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>High Pressure Laminates</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive " alt="">
 </div>
 </div>

</div>
<!-- //end tabs1 -->
  <div id="" class="tab-pane fade in  low-pressure-laminates">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Low Pressure Laminates</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive " alt="">
 </div>
 </div>

</div>
<!-- //end tabs2 -->

<div id="" class="tab-pane fade in  compact-laminates">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Compact Laminates</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive " alt="">
 </div>
 </div>

</div>
<!-- //end tabs3 -->

<div id="" class="tab-pane fade in  technical-laminates">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Technical Laminates</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive " alt="">
 </div>
 </div>

</div>
<!-- //end tabs4 -->
<div id="" class="tab-pane fade in  speciality-laminates">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Speciality Laminates</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive " alt="">
 </div>
 </div>

</div>
<!-- //end tabs5 -->
<div  class="tab-pane fade in  applications">
  <div class="products-inner-content">
 <div class="bg-1">
 <div style="
    display: table-cell;
    vertical-align: middle;
    position: absolute;
    top: 295px;
    left: 220px;
    right: 0px;
    margin: 0px auto;
">
  <div class="animate-flicker">Coming Soon...</div>
  </div>
 </div>
 </div>

</div>
<!-- //end tabs6 -->
</div>
</div>

</div>


  


    </div>



<div id="p1">

  <div class="section-white">
    <div class="container">
        <div class="row">
        <div class="col-md-3">
    <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>
     <div class="ads">
      <img src="assets/img/product-inner-ads-2.jpg" alt="product-inner-ads-2" class="img-responsive">
    </div>

   </div>

   <div class="products-inner-content">
     <div class="col-md-9">


 <div class="content-inner">
  <p>
   HPL is produced by saturating multiple layers of kraft paper with phenolic resin. A layer of printed décor paper is placed on top of the kraft paper before pressing. The resulting sandwich is fused together under heat and pressure (more than 1,000 PSI). The combination of heat and pressure creates a single, rigid laminated sheet. Thermosetting creates strong, irreversible bonds that contribute to HPL’s durability.
 </p>
 <img src="assets/img/product-inner-img-2.jpg" class="img-responsive  first-img" alt="">

 <p>HPL is laminated to a panel utilizing a variety of adhesives. Particleboard or MDF are the preferred substrate because they provide a stable, durable, consistent and economical foundation. Due to its durability,</p>

 <img src="assets/img/product-inner-ads-3.jpg" alt="" class="img-responsive second-img">
 <p>HPL is a common choice for horizontal surfaces including flooring, countertops and desktops. It also performs well in horizontal and vertical applications for high traffic settings such as hospitality, office furniture, healthcare, retail casework, commercial interiors and educational facilities</p>
</div>
      </div>
   </div>


    </div>
      <div class="row m-t-70"><p>The typical thicknesses available are 0.45mm, 0.6mm and 0.8mm with a standard size of 1220mm x 2440mm (8 x 4 feet).</p></div>
        <div class="row listingproductQuality">

          <table class="table table-bordered">
            <tr>
              <th>Name</th>
             <td><img src="assets/img/img-01.jpg" alt="img-01"></td>
             <td><img src="assets/img/img-02.jpg" alt="img-02"></td>
              <td><img src="assets/img/img-03.jpg" alt="img-03"></td>
             <td><img src="assets/img/img-04.jpg" alt="img-04"></td>
            </tr>

             <tr>
              <th>Thickness</th>
              <td> 0.45 mm ± 0.05 mm</td>
               <td> 0.60 mm ± 0.05 mm</td>
                <td> 0.80 mm ± 0.05 mm</td>
                 <td> 0.80 mm ± 0.05 mm</td>
            </tr>

             <tr>
              <th>Stock Size</th>
              <td> 1220 mm ± 2440 mm</td>
               <td> 1220 mm ± 2440 mm</td>
                <td> 1220 mm ± 2440 mm</td>
                 <td> 1220 mm ± 2440 mm</td>
            </tr>

             <tr>
              <th>Available Finihes</th>
              <td>Dull, Shine, Texture</td>
              <td>Dull, Shine, Texture, Ribwood, Super Matt, Canvas</td>
               <td>Dull, Shine, Texture, Ribwood, Super Matt, Canvas</td>
                 <td>Dull, Shine, Texture, Ribwood, Super Matt, Canvas</td>
            </tr>
          </table>
   
 
 
  
  </div>
    </div>
  </div>
<!-- //end tab1 1 -->

</div>

    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>