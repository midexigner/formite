<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Patterns</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp inner-gallery">
<div class="innerPorduct">
  <div class="container">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
<li role="presentation"><a href="new-finishes">New Finishes</a></li>
  <li role="presentation"><a href="marble">Marble</a></li>
  <li role="presentation"><a href="#">Metallica</a></li>
  <li role="presentation"><a href="woodgrain">WoodGrain</a></li>
 <li role="presentation"><a href="plain-colors">Solid Colors</a></li>
 <li role="presentation" class="active"><a href="patterns">Patterns</a></li>
 <li role="presentation"><a href="custom-laminates">Custom Laminates</a></li> 
</ul>
  <br>
  <br>
  <br>
      </div>
   <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>


</div>

<div class="col-md-9">
  <div class="products-inner-content">
    <div class="bg-1">
   <h3>Patterns</h3>
 <p>Observe our extensive range of eye-catching patterns.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/6015.jpg" alt="6015">
    </div>
      <div class="caption">
        <p>Color Code <span>6015</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/6015.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8410.jpg" alt="8410">
    </div>
      <div class="caption">
        <p>Color Code <span>8410</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8410.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8411.jpg" alt="8411">
    </div>
      <div class="caption">
        <p>Color Code <span>8411</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8411.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8425.jpg" alt="8425">
    </div>
      <div class="caption">
        <p>Color Code <span>8425</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8425.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8440.jpg" alt="8440">
    </div>
      <div class="caption">
        <p>Color Code <span>8440</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8440.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8446.jpg" alt="8446">
    </div>
      <div class="caption">
        <p>Color Code <span>8446</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8446.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8478.jpg" alt="8478">
    </div>
      <div class="caption">
        <p>Color Code <span>8478</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8478.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8602.jpg" alt="8602">
    </div>
      <div class="caption">
        <p>Color Code <span>8602</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8602.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8610.jpg" alt="8610">
    </div>
      <div class="caption">
        <p>Color Code <span>8610</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8610.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8612.jpg" alt="8612">
    </div>
      <div class="caption">
        <p>Color Code <span>8612</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8612.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8617.jpg" alt="8617">
    </div>
      <div class="caption">
        <p>Color Code <span>8617</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8617.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8626.jpg" alt="8626">
    </div>
      <div class="caption">
        <p>Color Code <span>8626</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8626.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8637.jpg" alt="8637">
    </div>
      <div class="caption">
        <p>Color Code <span>8637</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8637.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8640.jpg" alt="8640">
    </div>
      <div class="caption">
        <p>Color Code <span>8640</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8640.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8700.jpg" alt="8700">
    </div>
      <div class="caption">
        <p>Color Code <span>8700</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8700.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8704.jpg" alt="8704">
    </div>
      <div class="caption">
        <p>Color Code <span>8704</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8704.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8707.jpg" alt="8707">
    </div>
      <div class="caption">
        <p>Color Code <span>8707</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8707.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8709.jpg" alt="8709">
    </div>
      <div class="caption">
        <p>Color Code <span>8709</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8709.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8727.jpg" alt="8727">
    </div>
      <div class="caption">
        <p>Color Code <span>8727</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8727.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/pattern/8734.jpg" alt="8734">
    </div>
      <div class="caption">
        <p>Color Code <span>8734</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8734.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
  
</div>


 </div>


</div>

</div>


  


    </div>




    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>