<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp">
<div class="innerPorduct">
  <div class="container mb68">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation"><a href="high-pressure-laminates">High Pressure Laminates</a></li>
  <li role="presentation" class="active"><a href="low-pressure-laminates">Low Pressure Laminates</a></li>
  <li role="presentation"><a href="compact-laminates">Compact Laminates</a></li>

 <li role="presentation"><a href="technical-laminates">Technical Laminates</a></li>
 <li role="presentation"><a href="speciality-laminates">Speciality Laminates</a></li>
 <li role="presentation"><a href="applications">Applications</a></li> 
</ul>
      </div>
  


</div>

<div class="col-md-9">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Low Pressure Laminates</h3>
 <p>Best-matched edge solutions to interior decorations, low pressure laminates are ideal surfaces with pristine finishing and functionality. </p>
  <div class="tc-image-effect-shine">
 <img src="assets/img/product-inner-img-l1.jpg" class="img-responsive " alt="">
</div>
 </div>
 </div>


</div>

</div>


  


    </div>





  <div class="section-white">
    <div class="container">
        <div class="row">
        <div class="col-md-3">
    <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>
     <div class="ads">
      <img src="assets/img/product-inner-ads-2.jpg" alt="product-inner-ads-2" class="img-responsive">
    </div>

   </div>

   <div class="products-inner-content">
     <div class="col-md-9">


 <div class="content-inner">
  <p>
  LPLs are designed by Formite to resist chemical, heat and mechanical handling, with a competitive edge of high quality at low cost.
 </p>
  <div class="tc-image-effect-shine">
 <img src="assets/img/product-inner-img-l2.jpg" class="img-responsive  first-img" alt="">
</div>
<ul class="list-unstyled" type="A">
  <li>Laminated boards are manufactured on a short-cycle low pressure press.</li>
  <li>E2 glue is used to laminate its imported boards to cater to health concern.</li>
  <li>These low pressure laminated boards are ideal for use in furniture carcasses, vertical use, paneling, partitioning.</li>
</ul>

 <img src="assets/img/product-inner-ads-3.jpg" alt="" class="img-responsive second-img">

</div>
      </div>
   </div>


    </div>
      <div class="row m-t-70"><p>The typical thicknesses available are 0.45mm, 0.6mm and 0.8mm with a standard size of 1220mm x 2440mm (8 x 4 feet).</p></div>
        <div class="row listingproductQuality">

          <table class="table table-bordered table-width">
            <tr>
              <th>Name</th>
             <td><img src="assets/img/img-05.jpg" alt="img-05"></td>
             <td><img src="assets/img/img-06.jpg" alt="img-06"></td>
              <td><img src="assets/img/img-07.jpg" alt="img-07"></td>
            
            </tr>
 <tr>
              <th>Size</th>
              <td> 1220 x 2440 mm</td>
               <td> 1220 x 2440 mm</td>
                <td>1220 x 2440 mm</td>
              
            </tr>
             <tr>
              <th>Thickness</th>
              <td> 17 mm</td>
               <td>2.3 mm, 3.8 mm & 16 mm</td>
                <td>2.5 mm</td>
              
            </tr>

             <tr>
              <th>Decor</th>
              <td> 1 or 2</td>
               <td> 1 or 2</td>
                <td> 1 or 2</td>
               
            </tr>

             <tr>
              <th>Core</th>
              <td>N/A</td>
              <td>N/A</td>
               <td>N/A</td>
               
            </tr>

             <tr>
              <th>Overlay</th>
              <td>No</td>
              <td>No</td>
               <td>No</td>
               
            </tr>

             <tr>
              <th>Underlay</th>
              <td>No</td>
              <td>No</td>
               <td>No</td>
             
            </tr>

             <tr>
              <th>Tear Free</th>
              <td>Yes</td>
              <td>Yes</td>
               <td>Yes</td>
              
            </tr>

             <tr>
              <th>Finishes</th>
              <td>1</td>
              <td>2</td>
               <td>3</td>
               
            </tr>
          </table>
   
 
 
  
  </div>
    </div>
  </div>


    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>