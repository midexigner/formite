<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header our-mission" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/mission-banner.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Our Mission</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="content">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
        <h3 class="heading-title red">Mission</h3>
        <p>We bring joy to our customers by creating beautiful interiors &amp; enhancing their lifestyles while being the leading brand in the decorative surface industry,  offering innovative &amp; creative designs in both local and international markets.
</p>

      </div>
      </div>
      <div class="col-md-6">
        <div class="mission"><img src="assets/img/mission-230.jpg" alt="mission-img" class="img-responsive"></div>
      </div>
      
    </div>
<div class="row">
  <div class="col-md-12 mission-one">
    <img src="assets/img/mission-img-2.jpg" alt="mission"  class="wow zoomIn"  data-wow-duration="0.3s" data-wow-delay="0.4s">
  </div>
</div>


  </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>