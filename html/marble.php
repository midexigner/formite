<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Marble</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp inner-gallery">
<div class="innerPorduct">
  <div class="container">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation"><a href="new-finishes">New Finishes</a></li>
  <li role="presentation" class="active"><a href="marble">Marble</a></li>
  <li role="presentation"><a href="#">Metallica</a></li>
  <li role="presentation"><a href="woodgrain">WoodGrain</a></li>

 <li role="presentation"><a href="plain-colors">Solid Colors</a></li>
 <li role="presentation"><a href="patterns">Patterns</a></li>
 <li role="presentation"><a href="custom-laminates">Custom Laminates</a></li> 
</ul>
  <br>
  <br>
  <br>
</ul>
      </div>
   <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>


</div>

<div class="col-md-9">
  <div class="products-inner-content">
    <div class="bg-1">
   <h3>Marble</h3>
 <p>Formite brings you a whole new world of elegant and aesthetic marble textures.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/marble/8105.jpg" alt="8105">
      </div>
      <div class="caption">
        <p>Color Code <span>8105</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8105.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/marble/8106.jpg" alt="8106">
    </div>
      <div class="caption">
        <p>Color Code <span>8106</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8106.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/marble/8144.jpg" alt="8144">
    </div>
      <div class="caption">
        <p>Color Code <span>8144</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8144.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
       <div class="tc-image-anim-zoom-in">
      <img src="assets/img/gallery/marble/8145.jpg" alt="8145">
    </div>
      <div class="caption">
        <p>Color Code <span>8145</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8145.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   
  
</div>
 </div>


</div>

</div>


  


    </div>




    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>