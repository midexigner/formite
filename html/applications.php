<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp">
<div class="innerPorduct">
  <div class="container">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation"><a href="high-pressure-laminates">High Pressure Laminates</a></li>
  <li role="presentation"><a href="low-pressure-laminates">Low Pressure Laminates</a></li>
  <li role="presentation"><a href="compact-laminates">Compact Laminates</a></li>

 <li role="presentation"><a href="technical-laminates">Technical Laminates</a></li>
 <li role="presentation"><a href="speciality-laminates">Speciality Laminates</a></li>
 <li role="presentation" class="active"><a href="applications">Applications</a></li> 
</ul>
      </div>
  


</div>

<div class="col-md-9">
  <div class="products-inner-content">
 <div class="bg-1">

<div style="
    display: table-cell;
    vertical-align: middle;
    position: absolute;
    top: 295px;
    left: 220px;
    right: 0px;
    margin: 0px auto;
">
  <div class="animate-flicker">Coming Soon...</div>
  </div>
 <!--   <h3>Applications</h3>
 <p>HPL is considered to be one of the most durable decorative surface materials and is available with special performance properties including chemical, fire and wear resistance.</p>
 <img src="assets/img/product-inner-img-1.jpg" class="img-responsive " alt=""> -->
 </div>
 </div>


</div>

</div>


  


    </div>


    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>