<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp">
<div class="innerPorduct">
  <div class="container mb68">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation"><a href="high-pressure-laminates">High Pressure Laminates</a></li>
  <li role="presentation"><a href="low-pressure-laminates">Low Pressure Laminates</a></li>
  <li role="presentation"><a href="compact-laminates">Compact Laminates</a></li>

 <li role="presentation"><a href="technical-laminates">Technical Laminates</a></li>
 <li role="presentation" class="active"><a href="speciality-laminates">Speciality Laminates</a></li>
 <li role="presentation"><a href="applications">Applications</a></li> 
</ul>
      </div>
  


</div>

<div class="col-md-9">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Speciality Laminates</h3>
 <p><strong>Fibrite </strong>– Synthetic Resin Bonded Fabric Laminate, also acknowledged as ‘FIBRITE’ has its application for both mechanical and electrical usage. </p>
  <div class="tc-image-effect-shine">
 <img src="assets/img/product-inner-img-11.jpg" class="img-responsive " alt="">
</div>
 </div>
 </div>


</div>

</div>


  


    </div>





  <div class="section-white">
    <div class="container">
        <div class="row">
        <div class="col-md-3">
    <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>
     <div class="ads">
      <img src="assets/img/product-inner-ads-2.jpg" alt="product-inner-ads-2" class="img-responsive">
    </div>

   </div>

   <div class="products-inner-content">
     <div class="col-md-9">


 <div class="content-inner">
  <p>
    <ul class="list-unstyled" type="A">
      <li>It is non-corrosive and resistant to most acids and mild alkalis.</li>
      <li>Easy to machine to meet your requirements either for gears and other industrial uses.</li>
      <li>It possesses elasticity sufficient to absorb shocks and intermittent stresses as well as work well under heat and pressure.</li>
      <li>Available in 1 mm up to 50 mm sheet size 2440 x 1220 mm (8' x 4').</li>
    </ul>


 </p>
  <div class="tc-image-effect-shine">
 <img src="assets/img/product-inner-img-22.jpg" class="img-responsive  first-img" alt="">
</div>
 <p><strong>Laminated chalkboards</strong> – Available in a thickness of 2.5mm and a standard size of 2440 x 1220 mm (8' x 4'), our range of laminated chalkboards are high in demand for schools and education centers. Having an aesthetic appeal and economic pricing, Formite’s laminated chalkboards are easy to install and maintain.</p>

 <img src="assets/img/product-inner-ads-3.jpg" alt="" class="img-responsive second-img">
 <p><strong>Engraving Laminates</strong> – Available in an intricate interplay of colors and combinations, Formite produces laminates that are legible, sturdy and idyllic for use in name plates, pointers and name tags. </p>
 <p><strong>Marker Board</strong> – Breaking the nexus of monotony, top-quality “GOOF PROOF” marker boards are developed by Formite to expedite school, colleges, and universities of surfaces that can be cleaned right off</p>
</div>
      </div>
   </div>


    </div>
     
       
    </div>
  </div>


    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>