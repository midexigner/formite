<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Partners</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerpartner">
<div class="innerPartner">
  <div class="container mb68">

<div class="row ">
  
  <div class="col-md-3">
     <div class="partner-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation" class="title"><a href="#">All Partners</a></li>
  <li role="presentation" class="active"><a href="#">Interwood Mobel</a></li>
  <li role="presentation"><a href="#">Habbit</a></li>
 <li role="presentation"><a href="#">Offisys</a></li>
 <li role="presentation"><a href="#">PEL</a></li>
 <li role="presentation"><a href="#">Siemens</a></li> 
 <li role="presentation"><a href="#">Master Offisys</a></li> 
 <li role="presentation"><a href="#">Dimensions</a></li> 
 <li role="presentation"><a href="#">Torch Offices</a></li> 
 <li role="presentation"><a href="#">ChenOne</a></li> 
 <li role="presentation"><a href="#">Furnishing Pavillion</a></li> 
 <li role="presentation"><a href="#">Decent Furshishes</a></li> 
 <li role="presentation"><a href="#">GumCorp</a></li> 
</ul>
      </div>
  
 <img src="assets/img/partners-ads.jpg" alt="product-inner-ads-1" class="img-responsive">

</div>

<div class="col-md-9">
  <!-- <div class="partners-inner-content">
   <h2>Interwood Mobel</h2>
    <div class="tc-image-effect-shine">
 <img src="assets/img/partners-inner.jpg" class="img-responsive  mb20" alt="">
</div>
 <p class=" mb30"><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
 <div class="row mb30">
   <div class="col-md-6"><p>
     <strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </p></div>
   <div class="col-md-6">
     <div class="tc-image-effect-shine">
     <img src="assets/img/partners-inner-1.jpg" class="img-responsive " alt="">
   </div>
   </div>
 </div>
 <h3 class="mb18">Lorem ipsum dolor sit amet </h3>
 <p class="mb18">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisUt enim ad minim veniam, Ut enim ad minim veniam.</p>
  <div class="row">
   <div class="col-md-7">
     <div class="tc-image-effect-shine">
    <img src="assets/img/partners-inner-2.jpg" class="img-responsive " alt="">
  </div>
</div>
   <div class="col-md-5">
     <div class="tc-image-effect-shine">
     <img src="assets/img/partners-inner-3.jpg" class="img-responsive " alt="">
   </div>
   </div>
 </div>
 </div> -->


</div>

</div>


  


    </div>




    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>