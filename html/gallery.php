<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/gallery-banner.jpg" alt="gallery-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Gallery</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
 </div>

   
<!-- // being product section -->
<section id="gallery-content">
  <div class="container">
    <div class="row">
      <div class="heading wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="0.6s">
        <h3 class="heading-title">We accommodate your spaces with thoughtful designs </h3>
        <p>Prepare to be inspired by our exclusive range of mesmerizing patterns and colors. </p>
      </div>
    </div>
    <div class="row">
      <div class="inner-gallery-banner tc-image-effect-shine">
  <a href="new-finishes"><img src="assets/img/gallery-banner-1.jpg" alt="gallery-banner-1"></a>
        
      </div>

    </div>


      <div class="row gallery-service wow fadeInUp" data-wow-delay="200ms">
      <div class="gallery-list">
       <a href="marble">
          <div class="col-md-4"><img src="assets/img/gallery-img-7.jpg" class="img-responsive" alt="img-4">
    <div class="caption"><h3>Marble</h3><p>Formite brings you a whole new world of elegant and aesthetic marble textures.</p></div>
        </div>
       </a>
        </div>
        <div class="gallery-list">
          <a href="#">
        <div class="col-md-4"><img src="assets/img/gallery-img-8.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>METALLICA</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor magna aliqua.</p></div>
        </div>
      </a>
          </div>
        <div class="gallery-list">
          <a href="woodgrain">
        <div class="col-md-4"><img src="assets/img/gallery-img-9.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>WOODGRAIN</h3><p>Explore a perfectly appealing range of Wood grain colors and surfaces.</p></div>
        </div>
      </a>
      </div>
    </div>

    <div class="row gallery-service wow fadeInUp" data-wow-delay="200ms">
      <div class="gallery-list">
        <a href="plain-colors">
        <div class="col-md-4"><img src="assets/img/gallery-img-10.jpg" class="img-responsive" alt="img-4">
    <div class="caption"><h3>Solid Colors</h3><p>Delve into our ravishing selection of solid color range.</p></div>
        </div>
      </a>
        </div>
        <div class="gallery-list">
          <a href="patterns">
        <div class="col-md-4"><img src="assets/img/gallery-img-11.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>Patterns</h3><p>Observe our extensive range of eye-catching patterns.</p></div>
        </div>
      </a>
          </div>
        <div class="gallery-list">
          <a href="custom-laminates">
        <div class="col-md-4"><img src="assets/img/gallery-img-12.jpg" class="img-responsive" alt="img-4">
<div class="caption"><h3>Custom Laminates</h3><p>Coming Soon ...</p></div>
        </div>
      </a>
      </div>
    </div>

  </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>