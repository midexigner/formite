<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
     <title>Formite</title>
 <link rel='stylesheet' id='camera-css'  href='assets/camera-master/css/camera.css' type='text/css' media='all'> 
  <link href="https://fonts.googleapis.com/css?family=Seaweed+Script" rel="stylesheet">
  <?php require_once 'include/css.php'; ?>



</head>
<body>

  <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header timeline-achive" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/timeline-banner.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Our Partners</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>


   
<!-- // being product section -->
<section id="content" class="introduction">
  <div class="container">
    <div class="row">
      
        <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
        <h3>Commitment to Eminence </h3>
        <p>Formite partners with the leading brands to craft intricate interplays of innovative solutions that surpass client’s expectations. 
</p>
 </div>
      
    </div>
  </div>
  
</section> 
<br>
<section class="white-bg" id="content-two">
  <div class="container">
    <div class="row">
     <div class="col-md-4">
   <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
            <div data-thumb="assets/pslide-1.jpg" data-src="assets/pslide-1.jpg">
                <div class="camera_caption fadeFromBottom">
                   ASPIRING INVENTION
                </div>
            </div>
            <div data-thumb="assets/pslide-2.jpg" data-src="assets/pslide-2.jpg">
                <div class="camera_caption fadeFromBottom">
                   DELIVERING LUXURY
                </div>
            </div>
              <div data-thumb="assets/pslide-3.jpg" data-src="assets/pslide-3.jpg">
                <div class="camera_caption fadeFromBottom">
                   PROMISING VALUE
                </div>
            </div>
             <div data-thumb="assets/pslide-4.jpg" data-src="assets/pslide-4.jpg">
                <div class="camera_caption fadeFromBottom">
                   ICONIC CRAFTSMANSHIP
                </div>
            </div>
              <div data-thumb="assets/pslide-5.jpg" data-src="assets/pslide-5.jpg">
                <div class="camera_caption fadeFromBottom">
                  TECHNOLOGICAL SUPERIORITY  
                </div>
            </div>
          
        </div><!-- #camera_wrap_1 -->
      <div class="clearfix"></div>
       <div class="adsss">
         <img src="assets/img/timeline-2.jpg" alt="">
       </div>
     </div>
      <div class="col-md-8">
  <div id="latest-news" class="partners">
   
    <div class="row">
 <div class="row  wow fadeInUp  mb20" data-wow-delay="200ms">
        
     <div class="col-sm-6 col-md-6">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out  h-165">
          <a href="partners-inner">
        <img src="assets/img/partner-1.png" class="img-responsive" alt="latest-news-img-1">
      </a>
      </div>
       </div>
    </div>
        
        <div class="col-sm-6 col-md-6">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out  h-165">
          <a href="partners-inner">
        <img src="assets/img/partner-2.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
    
    
        </div> 


 <div class="row  wow fadeInUp  mb20" data-wow-delay="200ms">
        
     <div class="col-sm-6 col-md-6">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out  h-165">
          <a href="partners-inner">
        <img src="assets/img/partner-3.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
        <div class="col-sm-6 col-md-6">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out  h-165">
          <a href="partners-inner">
        <img src="assets/img/partner-4.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
    
    
        </div>


 <div class="row  wow fadeInUp  mb20" data-wow-delay="200ms">
        
     <div class="col-sm-6 col-md-6">
      <div class="thumbnail">
        <div class="tc-image-anim-zoom-out  h-165">
          <a href="partners-inner">
        <img src="assets/img/partner-5.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
        <div class="col-sm-6 col-md-6">
      <div class="thumbnail">
      <div class="tc-image-anim-zoom-out  h-165">
          <a href="partners-inner">
        <img src="assets/img/partner-6.png" class="img-responsive" alt="latest-news-img-1">
      </a>
    </div>
       </div>
    </div>
        
    
    
        </div> 


      



  </div>




        </div> 
    </div>
  </div>
  <br>
<br>

</section> 



<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 
 <script type='text/javascript' src='assets/camera-master/scripts/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='assets/camera-master/scripts/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='assets/camera-master/scripts/camera.min.js'></script>   
<script>
  
jQuery('#camera_wrap_1').camera({
        height: '450px',
        loader: 'bar',
        pagination: false,
        thumbnails: false
      });
</script>
</body>
</html>