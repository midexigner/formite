<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Marble</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->

</div>
   
<!-- // being product section -->
<section id="products-content" class="innerp inner-gallery">
<div class="innerPorduct">
  <div class="container">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked nav nav-tabs">
  <li role="presentation" class="active">
<a href="#marble" aria-controls="marble" role="tab" data-toggle="tab">Marble</a>
    </li>
  <li role="presentation">
<a href="#metallica" aria-controls="metallica" role="tab" data-toggle="tab">Metallica</a>
    </li>
  <li role="presentation">
<a href="#woodgrain" aria-controls="woodgrain" role="tab" data-toggle="tab">WoodGrain</a>
  </li>
 <li role="presentation">
<a href="#plain-colors" aria-controls="plain-colors" role="tab" data-toggle="tab">Solid Colors</a>
</li>
 <li role="presentation">
<a href="#patterns" aria-controls="patterns" role="tab" data-toggle="tab">Patterns</a>
</li>
 <li role="presentation">
<a href="#custom-laminates" aria-controls="marble" role="tab" data-toggle="tab">Custom Laminates</a>
  </li> 
</ul>
      </div>
   <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>


</div>

<div class="col-md-9">
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="marble"> <div class="products-inner-content">
    <div class="bg-1">
   <h3>Marble</h3>
 <p>Here you can have a look at all wide range of patterns and colors available to you</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/marble/8105.jpg" alt="8105">
      <div class="caption">
        <p>Color Code <span>8105</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8105.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/marble/8106.jpg" alt="8106">
      <div class="caption">
        <p>Color Code <span>8106</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8106.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/marble/8144.jpg" alt="8144">
      <div class="caption">
        <p>Color Code <span>8144</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8144.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/marble/8145.jpg" alt="8145">
      <div class="caption">
        <p>Color Code <span>8145</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/marble/8145.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   
  
</div>
 </div>
</div>
    <div role="tabpanel" class="tab-pane" id="metallica">
<div class="products-inner-content">
    <div class="bg-1">
   <h3>Metallica</h3>
 <p>Formite brings you a whole new world of elegant and aesthetic marble textures.</p>
 </div>
</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="woodgrain">
<div class="products-inner-content">
    <div class="bg-1">
   <h3>WoodGrain</h3>
 <p>Explore a perfectly appealing range of Wood grain colors and surfaces.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/2060.jpg" alt="2060">
      <div class="caption">
        <p>Color Code <span>2060</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/2060.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/2088.jpg" alt="2088">
      <div class="caption">
        <p>Color Code <span>2088</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/2088.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7102.jpg" alt="7102">
      <div class="caption">
        <p>Color Code <span>7102</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7102.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7116.jpg" alt="7116">
      <div class="caption">
        <p>Color Code <span>7116</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7116.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7124.jpg" alt="7124">
      <div class="caption">
        <p>Color Code <span>7124</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7124.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7155.jpg" alt="7155">
      <div class="caption">
        <p>Color Code <span>7155</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7155.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7168.jpg" alt="7168">
      <div class="caption">
        <p>Color Code <span>7168</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7168.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7180.jpg" alt="7180">
      <div class="caption">
        <p>Color Code <span>7180</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7180.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7182.jpg" alt="7182">
      <div class="caption">
        <p>Color Code <span>7182</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7182.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7184.jpg" alt="7184">
      <div class="caption">
        <p>Color Code <span>7184</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7184.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7196.jpg" alt="7196">
      <div class="caption">
        <p>Color Code <span>7196</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7196.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7198.jpg" alt="7198">
      <div class="caption">
        <p>Color Code <span>7198</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7198.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7241.jpg" alt="7241">
      <div class="caption">
        <p>Color Code <span>7241</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7241.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7352.jpg" alt="7352">
      <div class="caption">
        <p>Color Code <span>7352</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7352.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7357.jpg" alt="7357">
      <div class="caption">
        <p>Color Code <span>7357</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7357.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7361.jpg" alt="7361">
      <div class="caption">
        <p>Color Code <span>7361</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7361.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7363.jpg" alt="7363">
      <div class="caption">
        <p>Color Code <span>7363</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7363.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7401.jpg" alt="7401">
      <div class="caption">
        <p>Color Code <span>7401</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7401.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7405.jpg" alt="7405">
      <div class="caption">
        <p>Color Code <span>7405</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7405.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7406.jpg" alt="7406">
      <div class="caption">
        <p>Color Code <span>7406</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7406.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7407.jpg" alt="7407">
      <div class="caption">
        <p>Color Code <span>7407</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7407.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7413.jpg" alt="7413">
      <div class="caption">
        <p>Color Code <span>7413</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7413.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7415.jpg" alt="7415">
      <div class="caption">
        <p>Color Code <span>7415</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7415.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7417.jpg" alt="7417">
      <div class="caption">
        <p>Color Code <span>7417</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7417.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7418.jpg" alt="7418">
      <div class="caption">
        <p>Color Code <span>7418</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7418.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7419.jpg" alt="7419">
      <div class="caption">
        <p>Color Code <span>7419</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7419.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7423.jpg" alt="7423">
      <div class="caption">
        <p>Color Code <span>7423</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7423.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7424.jpg" alt="7424">
      <div class="caption">
        <p>Color Code <span>7424</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7424.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7486.jpg" alt="7486">
      <div class="caption">
        <p>Color Code <span>7486</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7486.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7495.jpg" alt="7495">
      <div class="caption">
        <p>Color Code <span>7495</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/woodgrain/7495.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/woodgrain/7591.jpg" alt="7591">
      <div class="caption">
        <p>Color Code <span>7591</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="gallery/woodgrain/7591.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 
 


 </div>


</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="plain-colors">
 <div class="products-inner-content">
    <div class="bg-1">
   <h3>Solid Colors</h3>
 <p>Delve into our ravishing selection of solid color range.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/8903.jpg" alt="8903">
      <div class="caption">
        <p>Color Code <span>8903</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/8903.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7014.jpg" alt="7014">
      <div class="caption">
        <p>Color Code <span>7014</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7014.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7015.jpg" alt="7015">
      <div class="caption">
        <p>Color Code <span>7015</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7015.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7017.jpg" alt="7017">
      <div class="caption">
        <p>Color Code <span>7017</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7017.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7019.jpg" alt="7019">
      <div class="caption">
        <p>Color Code <span>7019</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7019.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7021.jpg" alt="7021">
      <div class="caption">
        <p>Color Code <span>7021</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7021.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7022.jpg" alt="7022">
      <div class="caption">
        <p>Color Code <span>7022</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7022.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7023.jpg" alt="7023">
      <div class="caption">
        <p>Color Code <span>7023</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7023.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7024.jpg" alt="7024">
      <div class="caption">
        <p>Color Code <span>7024</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7024.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7043.jpg" alt="7043">
      <div class="caption">
        <p>Color Code <span>7043</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7043.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7046.jpg" alt="7046">
      <div class="caption">
        <p>Color Code <span>7046</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7046.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7047.jpg" alt="7047">
      <div class="caption">
        <p>Color Code <span>7047</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7047.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7052.jpg" alt="7052">
      <div class="caption">
        <p>Color Code <span>7052</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7052.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7053.jpg" alt="7053">
      <div class="caption">
        <p>Color Code <span>7053</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7053.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7054.jpg" alt="7054">
      <div class="caption">
        <p>Color Code <span>7054</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7054.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7056.jpg" alt="7056">
      <div class="caption">
        <p>Color Code <span>7056</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7056.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7072.jpg" alt="7072">
      <div class="caption">
        <p>Color Code <span>7072</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7072.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7073.jpg" alt="7073">
      <div class="caption">
        <p>Color Code <span>7073</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7073.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7076.jpg" alt="7076">
      <div class="caption">
        <p>Color Code <span>7076</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7076.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7077.jpg" alt="7077">
      <div class="caption">
        <p>Color Code <span>7077</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7077.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7100.jpg" alt="7100">
      <div class="caption">
        <p>Color Code <span>7100</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7100.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7600.jpg" alt="7600">
      <div class="caption">
        <p>Color Code <span>7600</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7600.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
     <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/7601.jpg" alt="7601">
      <div class="caption">
        <p>Color Code <span>7601</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/7601.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/plain/8507.jpg" alt="8507">
      <div class="caption">
        <p>Color Code <span>8507</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/plain/8507.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>



</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="patterns">

<div class="products-inner-content">
    <div class="bg-1">
   <h3>Patterns</h3>
 <p>Observe our extensive range of eye-catching patterns.</p>
 </div>
 <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/6015.jpg" alt="6015">
      <div class="caption">
        <p>Color Code <span>6015</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/6015.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8410.jpg" alt="8410">
      <div class="caption">
        <p>Color Code <span>8410</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8410.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8411.jpg" alt="8411">
      <div class="caption">
        <p>Color Code <span>8411</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8411.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8425.jpg" alt="8425">
      <div class="caption">
        <p>Color Code <span>8425</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8425.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8440.jpg" alt="8440">
      <div class="caption">
        <p>Color Code <span>8440</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8440.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8446.jpg" alt="8446">
      <div class="caption">
        <p>Color Code <span>8446</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8446.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8478.jpg" alt="8478">
      <div class="caption">
        <p>Color Code <span>8478</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8478.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8602.jpg" alt="8602">
      <div class="caption">
        <p>Color Code <span>8602</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8602.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8610.jpg" alt="8610">
      <div class="caption">
        <p>Color Code <span>8610</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8610.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8612.jpg" alt="8612">
      <div class="caption">
        <p>Color Code <span>8612</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8612.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8617.jpg" alt="8617">
      <div class="caption">
        <p>Color Code <span>8617</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8617.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8626.jpg" alt="8626">
      <div class="caption">
        <p>Color Code <span>8626</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8626.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8637.jpg" alt="8637">
      <div class="caption">
        <p>Color Code <span>8637</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8637.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8640.jpg" alt="8640">
      <div class="caption">
        <p>Color Code <span>8640</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8640.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8700.jpg" alt="8700">
      <div class="caption">
        <p>Color Code <span>8700</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8700.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail ">
      <img src="assets/img/gallery/pattern/8704.jpg" alt="8704">
      <div class="caption">
        <p>Color Code <span>8704</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8704.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8707.jpg" alt="8707">
      <div class="caption">
        <p>Color Code <span>8707</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8707.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8709.jpg" alt="8709">
      <div class="caption">
        <p>Color Code <span>8709</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8709.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8727.jpg" alt="8727">
      <div class="caption">
        <p>Color Code <span>8727</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8727.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
 <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="assets/img/gallery/pattern/8734.jpg" alt="8734">
      <div class="caption">
        <p>Color Code <span>8734</span></p>
      </div>
      <div class="hover">
        <a class="btn btn-default" href="assets/img/gallery/pattern/8734.jpg" data-lightbox="gallery-set"><i class="fa icon-search"></i></a>
      </div>
    </div>
  </div>
  
</div>


 </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="custom-laminates">
<div class="products-inner-content">
    <div class="bg-1">
   <h3>Custom Laminates</h3>
 <p>Formite brings you a whole new world of elegant and aesthetic marble textures.</p>
 </div>

    </div>
  </div>


</div>

</div>


  


    </div>




    </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>