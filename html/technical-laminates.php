<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner-1.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->
</div>

   
<!-- // being product section -->
<section id="products-content" class="innerp">
<div class="innerPorduct">
  <div class="container mb68">

<div class="row ">
  
  <div class="col-md-3">
     <div class="product-sidebar">
        <ul class="nav nav-pills nav-stacked">
  <li role="presentation"><a href="high-pressure-laminates">High Pressure Laminates</a></li>
  <li role="presentation"><a href="low-pressure-laminates">Low Pressure Laminates</a></li>
  <li role="presentation"><a href="compact-laminates">Compact Laminates</a></li>

 <li role="presentation" class="active"><a href="technical-laminates">Technical Laminates</a></li>
 <li role="presentation"><a href="speciality-laminates">Speciality Laminates</a></li>
 <li role="presentation"><a href="applications">Applications</a></li> 
</ul>
      </div>
  


</div>

<div class="col-md-9">
  <div class="products-inner-content">
 <div class="bg-1">
   <h3>Technical Laminates</h3>
 <p><strong>Grade P1</strong> – The paper based P1 (PFCP 201) has mechanical applications and mechanical properties far better than other PFCP types. </p>
  <div class="tc-image-effect-shine">
 <img src="assets/img/product-inner-img-111.jpg" class="img-responsive " alt="">
</div>
 </div>
 </div>


</div>

</div>


  


    </div>





  <div class="section-white">
    <div class="container">
        <div class="row">
        <div class="col-md-3">
    <div class="ads">
      <img src="assets/img/product-inner-ads-1.jpg" alt="product-inner-ads-1" class="img-responsive">
    </div>
     <div class="ads">
      <img src="assets/img/product-inner-ads-2.jpg" alt="product-inner-ads-2" class="img-responsive">
    </div>

   </div>

   <div class="products-inner-content">
     <div class="col-md-9">


 <div class="content-inner">
  <p>
   It is a preferred choice for its electrical properties at normal humidity and has various utilities including usage as insulation in pressure blocks for transformers, insulation in electric motors, switch gears and panels insulation barrier.
 </p>
  <div class="tc-image-effect-shine">
 <img src="assets/img/product-inner-img-222.jpg" class="img-responsive  first-img" alt="">
</div>
 <p><strong>Grade P2</strong> – The paper based P2 (PFCP 202) has low mechanical applications and mechanical properties comparatively but has high voltage applications at power frequencies and high electric strength under oil. It also has good electrical strength in air under normal humidity. </p>

 <img src="assets/img/product-inner-ads-3.jpg" alt="" class="img-responsive second-img">
 <p>It is a preferred choice for various utilities including usage as insulation between silicon windings of transformers, tap changers, switch gears and panels insulation barrier.</p>
</div>
      </div>
   </div>


    </div>
      
      
    </div>
  </div>


    </div>
	
</section>




<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>