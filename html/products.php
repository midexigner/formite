<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
	   <title>Formite</title>
	
  <?php require_once 'include/css.php'; ?>



</head>
<body>

	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 

require_once 'include/header.php';

?>

 <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide page-header" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="assets/img/product-banner.jpg" alt="product-banner">

          <div class="page-heading wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.2s">
            <h2>Products</h2>
          </div>
          
        </div>
       
 </div><!-- /.carousel -->

</div>
   
<!-- // being product section -->
<section id="products-content">
  <div class="container">
    <div class="row">
      <div class="heading wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.6s">
        <h3 class="red text-uppercase">Capture the essence of perfection with Formite</h3>
        <p>Explore our spectrum of tasteful designs and myriad possibilities reflecting the latest trends.</p>
      </div>
    </div>

    <div class="row product-section wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
      <div class="col-md-4">
        <div class="products-inner text-center">
           <div class="subheading">
          <a href="high-pressure-laminates"><h3>High Pressure Laminates</h3></a>
        </div>
        <div class="figcaption">
          <img src="assets/img/product-img-4.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Our ever growing brand, Formite, envisions on creating top-quality laminates by employing state of the art processes ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="high-pressure-laminates" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
          <a href="low-pressure-laminates"><h3>Low Pressure Laminates</h3></a>
          </div>
          <div class="figcaption">
          <img src="assets/img/product-img-5.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Best-matched edge solutions to interior decorations, low pressure laminates are ideal surfaces with pristine finishing and functionality ... </p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="low-pressure-laminates" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
           <a href="compact-laminates"><h3>Compact Laminates</h3></a>
         </div>
         <div class="figcaption">
          <img src="assets/img/product-img-6.jpg" alt="thumbnail" class="img-responsive">
            <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Originally having its significance as an embellished decorative grade material, compact laminates evolved as industrial grade  ... </p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="compact-laminates" class="more-info">explore</a>
            </div>
          </div><!-- end caption -->
          </div>
          </div>
        
        </div>
      </div>
    </div> 

    <div class="row product-section wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.8s">
      <div class="col-md-4">
        <div class="products-inner text-center">
           <div class="subheading">
          <a href="technical-laminates"><h3>Technical Laminates</h3></a>
        </div>
        <div class="figcaption">
          <img src="assets/img/product-img-004.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Grade P1 – The paper based P1 (PFCP 201) has mechanical applications and mechanical properties far better than other PFCP types ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="technical-laminates" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
          <a href="speciality-laminates"><h3>Speciality Laminates</h3></a>
          </div>
          <div class="figcaption">
          <img src="assets/img/product-img-005.jpg" alt="thumbnail" class="img-responsive">
          <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Fibrite – Synthetic Resin Bonded Fabric Laminate, also acknowledged as ‘FIBRITE’ has its application for both mechanical and ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="speciality-laminates" class="more-info">explore</a>
            </div>
            </div>
          </div><!-- end caption -->
        </div>
        </div>
      </div>
       <div class="col-md-4">
        <div class="products-inner text-center">
          <div class="subheading">
           <a href="applications"> <h3>Application</h3></a>
         </div>
         <div class="figcaption">
          <img src="assets/img/product-img-006.jpg" alt="thumbnail" class="img-responsive">
            <div class="caption">
            <div class="caption-heading visible-lg">
              <p>Coming Soon ...</p>
           
            
            <div class="btn-trigger">
              <i class="formite-link"></i>
              <a href="applications" class="more-info">explore</a>
            </div>
          </div><!-- end caption -->
          </div>
          </div>
        
        </div>
      </div>
    </div>



  </div>
	
</section>


<?php 

require_once 'include/footer.php';
require_once 'include/js.php'; ?> 	
</body>
</html>